// Copyright (C) 2020 Ferdinand Majerech
// 
// This file is part of Timedilator.
//
// Timedilator is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Timedilator is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Timedilator.  If not, see <http://www.gnu.org/licenses/>.

import std.algorithm.iteration;
import std.algorithm.searching: count;
import std.array;
import std.conv:to;
import std.exception;
import std.file;
import std.format;
import std.getopt;
import std.process;
import std.stdio;
import std.typecons;
import stdx.allocator;
import stdx.allocator.gc_allocator;
import stdx.allocator.mallocator;

import timedilator.datasource;
import timedilator.panes;
import timedilator._scope: StreamID;
import timedilator.scopemanager;
import timedilator.tui;


/// Main allocator used by the program.
alias Allocator = Mallocator;

/// We don't produce too much garbage - 1 GC thread should be enough.
extern (C) __gshared string[] rt_options = ["gcopt=parallel:1"];


/** Implements Command code specific to single command.
 *
 * Passed to Command constructor. Command calls methods of CommandBackend to
 * execute command specific functionality.
 *
 * TODO: this design is needlessly clever - avoiding OOP resulted in extra
 * complexity. Deriving individual commands from a parent Command class would
 * probably be simpler.
 */
interface CommandBackend
{
    /** Get file path for data source with specified index.
     *
     * Params:
     * index       = Index of the data source to look for.
     * baseOptions = CLI/config options shared by all commands. May affect path building.
     *
     * Returns: path of the data source file.
     */
    string sourcePath( in size_t index, in Command.Options baseOptions );

    /** Initialize command-specific code at the beginning of command main().
     *
     * Params: base = Base command, for access to scope manager, TUI, options
     *                etc; a lot of stuff may be used during backend
     *                initialization.
     *
     * Returns: PaneManager initialized with panes for this command, or NULL on failure.
     */
    PaneManager mainInit( Command base );

    /** Called when Command.main() exists to deinitialize any command-specific resources.
     *
     * Returns: 0 on success, non-zero on failure.
     */
    int mainShutdown( TUI tui );

    /** Called when a new DataSource is found (e.g. because profiled program starts recording from a new thread).
     *
     * Can be used to initialize code to view the data source.
     *
     * Params:
     * base      = Base command, for access to data sources and scope manager among other things.
     * streamIdx = Index of the data source / stream.
     */
    void sourceDetected( Command base, in StreamID streamIdx );
}

// /// `timedilator report` - processes profiling data from a finished run of a program.
// class CommandBackendReport: CommandBackend
// {
//     // XXX Implement this command ASAP.
// }

/** `timedilator attach` - works on profiling output currently being written by a program.
 */
class CommandBackendAttach: CommandBackend
{
    import std.path;
private:
    /// CLI options specific for this command.
    struct Options
    {
        /// Directory the nanoprof data files are written to by the program.
        string dataDir = ".";
    }

    /// CLI/config options.
    Options options;

public:
    /** Construct a CommandBackendAttach.
     *
     * Params:
     * rhsOptions = CLI options.
     */
    this( Options rhsOptions )
    {
        stderr.writefln( "CommandBackendAttach.this %s", rhsOptions );
        options = rhsOptions;
    }

    string sourcePath( in size_t index, in Command.Options baseOptions )
    {
        // Look for dataPathBase within dataDir
        return buildPath( options.dataDir, baseOptions.dataPathBase.replace( "{STREAM}", index.to!string ) );
    }

    PaneManager mainInit( Command base )
    {
        stderr.writefln( "CommandBackendAttach.mainInit" );
        import timedilator._scope;
        // TODO CLI option to set which stream to view (or maybe just view all of them)
        // TODO also support switching the viewed stream (probably with 1 pane per stream,
        //      and a fullscreen layout in pane manager)
        const StreamID streamToView = 0;
        auto paneManager = new PaneManager();
        paneManager.addPane( new SimpleStreamPane( base.scopeManager, streamToView ) );
        return paneManager;
    }

    int mainShutdown( TUI tui ) { return 0; }

    void sourceDetected( Command base, in StreamID streamIdx )
    {
        base.paneManager.addPane( new SimpleStreamPane( base.scopeManager, streamIdx ) );
    }

    /** Parse and return CLI options specific for this command.
     *
     * Params:
     * tui  = TUI for help info display.
     * args = Args to parse.
     * 
     * Return: Parsed options, NULL on error.
     */
    static Nullable!Options cliParse( TUI tui, string[] args )
    {
        stderr.writefln( "CommandBackendAttach.cliParse %s", args );

        Options result;
        // Parse CLI.
        auto helpInfo = getopt( args,
            std.getopt.config.bundling,
            std.getopt.config.caseSensitive,
            std.getopt.config.stopOnFirstNonOption );


        // Interpret what we cannot parse as the profile data output directory of the profiled process.
        if( args.length <= 1)
        {
            screenMsg( tui, StatusType.Error, "CLI error", "no profile data directory specified. Example: `timedilator attach path/to/profile/data/writen/by/process`" );
            return Nullable!Options();
        }
        result.dataDir = args[1];
        stderr.writeln( "timedilator attach dataDir: ", result.dataDir );

        // Write help info if requested.
        if( helpInfo.helpWanted )
        {
            auto help = appender!string();
            // XXX detailed help info describing the 'attach' command
            auto header = "timedilator attach";
            defaultGetoptFormatter( help, header, helpInfo.options );
            screenMsg( tui, StatusType.Info, "Help information", help.data );
            return Nullable!Options();
        }

        return nullable( result );
    }
}

/** `timedilator run` - runs a program and processes its profiling output in real time.
 */
class CommandBackendRun: CommandBackend
{
    import std.range;
private:
    /// CLI options specific for this command.
    struct Options
    {
        version(Posix)
        {
            /// Path to redirect to the run command's stdin.
            string pathStdin = "/dev/null";
        }
        else
        {
            //TODO
            static assert( "Find a /dev/null equivalent for Windows for default subprocess stdin path!" );
        }

        /// Path to redirect the run command's stdout to.
        string pathStdout;
        /// Path to redirect the run command's stderr to.
        string pathStderr;
        /// Command of the profiled program to run.
        string[] commandToRun;
    }

    /// CLI/config options.
    Options options;

    /// Pid of the program we're running/profiling.
    Pid pid;

public:
    /** Construct a CommandBackendRun.
     *
     * Params:
     * rhsOptions = CLI options.
     */
    this( Options rhsOptions )
    {
        stderr.writefln( "CommandBackendAttach.this %s", rhsOptions );
        options = rhsOptions;
    }

    string sourcePath( in size_t index, in Command.Options baseOptions )
    {
        return baseOptions.dataPathBase.replace( "{STREAM}", index.to!string );
    }

    PaneManager mainInit( Command base )
    {
        stderr.writefln( "CommandRun.mainInit" );

        try
        {
            auto processIn  = File( options.pathStdin, "r" );
            auto processOut = File( options.pathStdout, "w" );
            auto processErr = File( options.pathStderr, "w" );

            // TODO draw frames/be interactive while program is starting:
            // startProgram should be part of mainLoop. Profile TUI w/ nanoprof
            // first to know exactly what's causing the 'lag' at startup.

            if( 0 != startProgram( processIn, processOut, processErr, pid, base ) )
            {
                return null;
            }
        }
        // Thrown by File constructors
        catch( ErrnoException e )
        {
            screenMsg( base.tui, StatusType.Error, "ERROR OPENING FILE", "%s".format( e ) );
            return null;
        }

        import timedilator._scope;
        // TODO CLI option to set which stream to view (or maybe just view all of them)
        // TODO also support switching the viewed stream (probably with 1 pane per stream,
        //      and a fullscreen layout in pane manager)
        const StreamID streamToView = 0;
        auto paneManager = new PaneManager();
        paneManager.addPane( new SimpleStreamPane( base.scopeManager, streamToView ) );
        return paneManager;
    }

    int mainShutdown( TUI tui )
    {
        enum waitSecs = 1;

        try
        {
            // Starts as 'not terminated'.
            std.typecons.Tuple!(bool, "terminated", int, "status") waitStatus;
            // Waits waitFrames frames and returns true if the process is dead, false otherwise.
            bool waitForProcessToDie()
            {
                const waitFrames = waitSecs * tui.framerate;
                for( size_t frame = 0; !waitStatus.terminated && frame < waitFrames; ++frame )
                {
                    waitStatus = pid.tryWait();
                    tui.frame();
                }
                return waitStatus.terminated;
            }

            tui.programStatus( StatusType.Info, "1: Shutting down the process nicely...");
            // On Windows, we just check if the process is already dead,
            // on POSIX we also send it SIGINT.
            version(Posix)
            {
                import core.sys.posix.signal;
                std.process.kill( pid, SIGINT );
            }
            if( waitForProcessToDie() ) { return 0; }


            // No nice way to terminate a process on Windows (? really ?).
            version(Posix)
            {
                tui.programStatus( StatusType.Info, "2: Shutting down the process forcefully..." );
                import core.sys.posix.signal;
                std.process.kill( pid, SIGTERM );

                if( waitForProcessToDie() ) { return 0; }
            }


            // If the process is still alive, kill it.
            tui.programStatus( StatusType.Info, "3: Shutting down the process brutally");
            version(Posix)
            {
                import core.sys.posix.signal;
                std.process.kill( pid, SIGKILL );
            }
            version(Windows)
            {
                std.process.kill( pid );
            }
            if( !waitForProcessToDie() )
            {
                screenMsg( tui, StatusType.Error, "ERROR SHUTTING DOWN PROFILED PROGRAM", "Profiled program refused to die." );
            }
        }
        catch( ProcessException e )
        {
            screenMsg( tui, StatusType.Error, "ERROR SHUTTING DOWN PROFILED PROGRAM", "%s".format( e ) );

            return 1;
        }

        return 0;
    }

    void sourceDetected( Command base, in StreamID streamIdx )
    {
        base.paneManager.addPane( new SimpleStreamPane( base.scopeManager, streamIdx ) );
    }

    /** Parse and return CLI options specific for this command.
     *
     * Params:
     * tui  = TUI for help info display.
     * args = Args to parse.
     * 
     * Return: Parsed options, NULL on error.
     */
    static Nullable!Options cliParse( TUI tui, string[] args )
    {
        stderr.writefln( "CommandBackendRun.cliParse %s", args );

        Options result;
        // Parse CLI.
        auto helpInfo = getopt( args,
            std.getopt.config.bundling,
            std.getopt.config.caseSensitive,
            std.getopt.config.stopOnFirstNonOption,

            "stdin|i",  "Path to file to use as standard input for profiled process. Default: /dev/null", &result.pathStdin,
            "stdout|o", "Path to file to use as standard output for profiled process. Default: PROCESS_NAME.out ", &result.pathStdout,
            "stderr|e", "Path to file to use as standard output for profiled process. Default: PROCESS_NAME.err ", &result.pathStderr
            );

        // Interpret what we cannot parse as the command to run.
        result.commandToRun = args[1 .. $];
        stderr.writeln( "timedilator run commandToRun: ", result.commandToRun );
        if( result.commandToRun.empty )
        {
            screenMsg( tui, StatusType.Error, "CLI error", "no command to run specified. Example: `timedilator run command`" );
            return Nullable!Options();
        }
        // Defaults for these paths are based on the commandToRun option.
        if( result.pathStdout is null ) { result.pathStdout = result.commandToRun[0] ~ ".out"; }
        if( result.pathStderr is null ) { result.pathStderr = result.commandToRun[0] ~ ".out"; }

        // Write help info if requested.
        if( helpInfo.helpWanted )
        {
            auto help = appender!string();
            // XXX detailed help info describing the 'run' command
            auto header = "timedilator run";
            defaultGetoptFormatter( help, header, helpInfo.options );
            screenMsg( tui, StatusType.Info, "Help information", help.data );
            return Nullable!Options();
        }

        return nullable( result );
    }

private:
    /** If there are profiling output files from a previous run, delete them.
     *
     * Called before launching profiled process so we don't end up reading parts
     * of data from a previous run.
     */
    void clearOldProfileOutputs( DataSource[] dataSources, in Command.Options baseOptions)
    {
        auto names = dataSources.length.iota.map!(i => sourcePath(i, baseOptions)).filter!(p => p.exists && p.isFile);
        foreach( name; names ) { name.remove(); }
    }


    /** Start the profiled program.
     *
     * Params:
     * processIn  = File to use as stdin for the process.
     * processOut = File to use as stdout for the process.
     * processErr = File to use as stderr for the process.
     * pid        = Pid of the started process will be written here.
     * base       = Base Command, to access data source path info.
     *
     * Returns: 0 on success, non-zero on failure to start the process.
     */
    int startProgram( ref File processIn, ref File processOut, ref File processErr, out Pid pid, Command base )
    {
        try
        {
            // TODO instead of using base fields directly, use them through getters.
            //      maybe split command and backends to separate files to enforce this
            clearOldProfileOutputs( base.dataSources, base.options );
            pid = spawnProcess( options.commandToRun, processIn, processOut, processErr );
            return 0;
        }
        catch( FileException e )
        {
            screenMsg( base.tui, StatusType.Error, "ERROR CLEARING OLD PROFILE DATA", "%s".format( e ) );
            return 1;
        }
        catch( ProcessException e )
        {
            screenMsg( base.tui, StatusType.Error, "ERROR STARTING PROCESSED PROGRAM", "%s".format( e ) );
            return 1;
        }
    }

}

/** Base command code, implementing logic common to all commands.
 *
 * Logic specific to individual commands is implemented by CommandBackend
 * implementation passed to Command ctor.
 *
 */
class Command
{
public:
    /// CLI options common for all commands.
    struct Options
    {
        /// File path (in dataDir) for the nanoprof data files written by the program.
        string dataPathBase = "data.{STREAM}.nanoprof";
        /// Desired terminal framerate.
        uint framerate = 12;
        /// Maximum number of input streams (usually profiled program threads).
        size_t maxStreams = 256;
        /// Every streamCheckPeriod seconds we check if the profiled program has started writing new streams.
        size_t streamCheckPeriod = 2;
    }

private:
    /// CLI/config options common for all commands.
    Options options;
    /// Terminal window, drawing, input, framerate management.
    TUI tui;
    /// Nanoprof streams (e.g. files) we're reading - written by the profiled program.
    DataSource[] dataSources;
    /// Allocator for manual memory management.
    Allocator allocator;
    /// Manages nanoprof scope data loaded from the data sources.
    ScopeManager scopeManager;
    /// Command-specific options and implementation.
    CommandBackend backend;
    /// Acts as a 'window manager' for the TUI, determining layout/focus of panes.
    PaneManager paneManager;

    /** Construct a Command.
     *
     * Params:
     * rhsTUI     = Terminal window/drawing/input.
     * rhsOptions = CLI options.
     * rhsBackend = Implements command-specific logic.
     */
    this( TUI rhsTUI, Options rhsOptions, CommandBackend rhsBackend )
    {
        stderr.writefln( "Command.this %s", rhsOptions );
        tui           = rhsTUI;
        options       = rhsOptions;
        backend       = rhsBackend;
        dataSources   = new DataSource[options.maxStreams];
        scopeManager  = new ScopeManager( options.maxStreams, allocatorObject( allocator ) );
        tui.framerate = options.framerate;
    }

    /** Command entry point.
     * 
     * Works as a main() function - initializes the backend and calls the main event loop.
     *
     * Returns: 0 on success, non-zero on error.
     */
    int main()
    {
        tui.programStatus( StatusType.Info, "OK" );

        paneManager = backend.mainInit( this );
        if( paneManager is null )
        {
            return 1;
        }

        mainLoop( paneManager );

        return 0;
    }

    /** Read more data (if any) from all open streams, in a round-robin fashion.
     *
     * Returns: number of nanoprof events read.
     */
    size_t readStreams()
    {
        size_t totalEventsRead = 0;

        foreach( ref source; dataSources.filter!(s => s !is null && s.ok) )
        {
            const eventCountBefore = source.stats.eventsRead;
            // Read more events.
            if( source.readForward( scopeManager ))
            {
                // Successful read - continue.
                totalEventsRead += source.stats.eventsRead - eventCountBefore;
                continue;
            }

            // Handle errors.
            import timedilator.error;
            const TUIError error = source.error;
            screenMsg( tui, StatusType.Error, "ERROR READING PROFILE DATA", error.humanMessage );
            // Disable the source, but scopeManager can still work with already loaded data.
            source = null;
        }
        return totalEventsRead;
    }

    /** Update the statusline, showing how many events / sources we have.
     *
     * Called once per frame.
     */
    void updateStatusLine()
    {
        // After reading data we check if we're at the (current) end of all streams.
        const sourceCount = dataSources.count!(s => s !is null);
        if( 0 == sourceCount )
        {
            tui.programStatus( StatusType.Info, 
                "Couldn't find profiling output written by the application. Tried '%s' and others with numbers up to %s"
                .format( backend.sourcePath( 0, options ), options.maxStreams ) );
        }
        const eventsRead   = dataSources.map!(s => s is null ? 0 : s.stats.eventsRead ).sum;
        const eventsReread = dataSources.map!(s => s is null ? 0 : s.stats.eventsReread ).sum;
        tui.programStatus( StatusType.Info, "%s events, %s reread from %s sources"
                    .format( eventsRead, eventsReread, sourceCount ) );
    }

    /** Check if there are any new source files (e.g. for newly created threads).
     *
     * Called every streamCheckPeriod seconds.
     */
    void detectSources()
    {
        const size_t dataSourcesCheckFrames = options.streamCheckPeriod * tui.framerate;
        if( tui.frameIndex % dataSourcesCheckFrames != 0 )
        {
            return;
        }
        // Only check data source (thread) indices aren't already reading data for.
        foreach( idx, ref source; dataSources ) if( source is null )
        {
            // TODO Check files written by process (inotify?) w/ no periodic
            // checks. Linux/unix? specific, other OS still need this.

            const filename = backend.sourcePath( idx, options );
            if( !filename.exists || !filename.isFile )
            {
                continue;
            }

            // 64k threads ought to be enough for everybody.
            assert( idx < ushort.max && "Only up to 2^^16 data sources supported at the moment" );
            // File exists. Build datasource reading it.
            source = new DataSourceFile( { return File( filename, "r" ); }, cast(ushort)idx );
            backend.sourceDetected( this, cast(StreamID)idx );

            tui.programStatus( StatusType.Info, "Opened data source '%s'".format( filename ) );
        }
    }

    /** Main program event loop.
     *
     * Params:
     * paneManager = Manages layout of view panes and which pane gets user input.
     */
    void mainLoop( PaneManager paneManager )
    {
        bool run = true;
        while( run )
        {
            detectSources();

            size_t eventsRead = 0;
            // Read events in a loop for as long as we get new events and there 
            // is time left in current frame.
            do
            {
                eventsRead = readStreams();
            }
            while( tui.framePercentRemaining > 50 && eventsRead > 0 );

            updateStatusLine();
            // Pass input to the pane manager, update and draw it.
            import scone;
            run = tui.input( (InputEvent event){ paneManager.processInputEvent( event ); } );
            paneManager.frame( tui.windowWidth, tui.windowHeight );
            foreach( pane; paneManager.panes )
            {
                pane.draw( tui );
            }

            // Draw the TUI and sleep if we ended the frame too early.
            tui.frame();
        }
    }
}

/** Draw a full-screen message and run an event loop until the user quits.
 *
 * Used for Really Important Information, such as telling the user that we're fucked and are shutting down.
 *
 * Params:
 * tui     = TUI for terminal acccess.
 * status  = Program status (error or info).
 * context = Context for the message - will be printed to the statusline.
 * message = Message to print to the screen.
 */
void screenMsg( TUI tui, in StatusType status, in string context, in string message )
{
    import core.thread;
    tui.screen = message;
    tui.programStatus( status, context );
    stderr.writefln( "screenMsg %s %s %s", status, context, message );

    bool run = true;
    while( run )
    {
        run = tui.input();
        tui.frame();
    }
}

/** Process CLI options, determine subcommand and run that subcommand.
 *
 * Params:
 * args = Program CLI args.
 *
 * Returns: 0 on success, non-zero on failure.
 */
int processCLI( string[] args )
{
    import core.exception;
    stderr.writefln( "processCLI %s", args );

    auto tui = new TUI();

    // Options used by all commands.
    Command.Options sharedOptions;

    try 
    {
        auto helpInfo = getopt( args, 
            std.getopt.config.bundling,
            std.getopt.config.caseSensitive,
            std.getopt.config.stopOnFirstNonOption,
            "framerate|f",           "Text refresh rate. Decrease if your terminal is slow. Can be 1 to 240. Default: 10", &sharedOptions.framerate,
            "data-path|d",           "Base path for nanoprof stream files written by the running program. String {STREAM} is replaced by a integer index for each stream. Default: %s".format( sharedOptions.dataPathBase ), &sharedOptions.dataPathBase,
            "max-streams|s",         "Maximum data streams (usually threads) written by profiled application. Default: 256", &sharedOptions.maxStreams,
            "stream-check-period|p", "Period to check for new data streams, in seconds. Default: 2", &sharedOptions.streamCheckPeriod,
            );

        // Write help info if requested.
        if( helpInfo.helpWanted )
        {
            auto help = appender!string();
            // XXX detailed help info describing available commands and common options
            auto header = "timedilator run";
            defaultGetoptFormatter( help, header, helpInfo.options );
            screenMsg( tui, StatusType.Info, "Help information", help.data );
            return 0;
        }

        // Interpret the first arg we cannot parse as the subcommand.
        auto commandName = args[1];
        stderr.writeln( "timedilator subcommand: ", commandName );

        switch( commandName )
        {
            case "report":
                // TODO report will look for 'data.$THREAD.nanoprof' files if not specified
                //      will have different default view than 'run' - since it's non-realtime
                assert( false, "TODO" );
            case "attach":
                auto backendOptions = CommandBackendAttach.cliParse( tui, args[1 .. $] );
                if( backendOptions.isNull )
                {
                    return 1;
                }
                auto backend        = new CommandBackendAttach( backendOptions.get() );
                auto commandShared  = new Command( tui, sharedOptions, backend );
                return commandShared.main();
            case "run":
                auto backendOptions = CommandBackendRun.cliParse( tui, args[1 .. $] );
                if( backendOptions.isNull )
                {
                    return 1;
                }
                auto backend        = new CommandBackendRun( backendOptions.get() );
                auto commandShared  = new Command( tui, sharedOptions, backend );
                return commandShared.main();
            case "help":
                // XXX global, command-independent help
                assert( false, "TODO" );
            default:
                screenMsg( tui, StatusType.Info, "Unknown command",
                        "Unknown command '%s'. Try 'help', 'run' or 'report'".format( commandName ) );
            return 1;
        }
    }
    // Thrown by getopt on unrecognized option
    catch( GetOptException e )
    {
        screenMsg( tui, StatusType.Error, "CLI OPTION ERROR", "%s. Try '-h' for help".format( e.msg ) );
        return 1;
    }
    // Need to display this, otherwise it'll get overdrawn by TUI output.
    catch( Exception e )
    {
        screenMsg( tui, StatusType.Error, "UNHANDLED EXCEPTION", "%s".format( e ) );
        return 1;
    }
    catch( AssertError e )
    {
        screenMsg( tui, StatusType.Error, "ASSERT ERROR", "%s".format( e ) );
        return 1;
    }
    catch( Error e )
    {
        screenMsg( tui, StatusType.Error, "UNHANDLED FATAL ERROR", "%s".format( e ) );
        return 1;
    }
}

/// Program entry point.
int main( string[] args )
{
    enum errorLogPath = "timedilator.log";
    try
    {
        auto errorLog = File( errorLogPath, "w" );
        // Redirect stderr as we can't see it in console in a TUI application.
        stderr = errorLog;
        return processCLI( args );
    }
    catch( ErrnoException e )
    {
        stderr.writefln( "Failed to open log file '%s'", errorLogPath );
        return 1;
    }
}
