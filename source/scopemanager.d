// Copyright (C) 2020 Ferdinand Majerech
//
// This file is part of Timedilator.
//
// Timedilator is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Timedilator is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Timedilator.  If not, see <http://www.gnu.org/licenses/>.

module timedilator.scopemanager;

import std.algorithm;
import std.stdio;
import stdx.allocator;
import containers.dynamicarray;
import containers.ttree;

import timedilator.nanoprof;
import timedilator._scope;


/** Stores profile data in form of scopes.
 *
 * This is the main 'storage' of nanoprof data.
 * 
 * ScopeManager consists of multiple Streams (independent profiling data
 * streams, such as from different threads of profiled process), which are
 * organized into Tracks (one per nest level).  Each Track acts as an ordered
 * sequences of scopes.
 *
 * ScopeManager provides access to scopes (`trackView()`) and new scopes can be
 * added to it (`scopeStart()`).
 *
 * TODO: a scope can be forgotten when we use too much memory, and re-loaded
 * when we need to access it again.
 */
class ScopeManager
{
private:
    /// Each item of this array stores Scopes (in Tracks) of a single nanoprof stream.
    StreamArray streams;

public:
    /** Construct a ScopeManager.
     *
     * Params:
     * streamCount = Maximum number of streams we can get data from.
     * allocator   = Allocator for scope allocation.
     */
    this( in size_t streamCount, IAllocator allocator )
    {
        foreach( i; 0 .. streamCount )
        {
            streams ~= new Stream( allocator );
        }
    }

    /** Get the highest Time value of all scopes added so far for given stream.
     *
     * This is the (profiling, not real) time of the last update (so far) for a given stream.
     */
    Time lastUpdateTime( const StreamID stream ) const { return streams[stream].lastUpdateTime_; }

    /** Get the lowest Time value of all scopes added to the scope manager.
     *
     * This is the (profiling, not real) time of the first update of all streams.
     */
    Time startTime() const
    {
        return streams[].map!(s => s.startTime).minElement;
    }

    /** Get a view of 'large enough' scopes in a time window of specified track of specified stream.
     *
     *
     * Scopes in the view are copied to memoryToUse. If memoryToUse is used up, it
     * is immediately returned with as much data as could be written to it.
     * This is to make parallel implementation easier.
     *
     * Params:
     * stream         = Stream we want a view of.
     * trackLevel     = Nest level / track we want scopes from.
     * startTime      = Time window start - scopes that end before this time will not be returned.
     * endTime        = Time window end - scopes that start after this time will not be ignored.
     * timeResolution = Minimum size of scopes to return - smaller scopes will
     *                  not be returned to save memory/bandwidth. Note that
     *                  scopes smaller than this size may be *merged* into a
     *                  scope with timepoint ID TimepointIDMerged - and such
     *                  merged scopes may be returned.
     * memoryToUse    = Returned scopes are written to this buffer. If there are
     *                  more scopes than memoryToUse.length, first
     *                  memoryToUse.length scopes are returned.
     *
     * Returns: slice of memoryToUse containing all written scopes.
     */
    Scope[] trackView(
        in StreamID stream,
        in NestLevel trackLevel,
        in Time startTime,
        in Time endTime,
        in Time timeResolution,
        Scope[] memoryToUse ) const
    {
        auto result = streams[stream].trackAtLevel( trackLevel )
                              .copyView( startTime, endTime, timeResolution, memoryToUse );
        return result;
    }

    /** Start a new open scope and end any previous scopes at equal or deeper nest levels in corresponding streams.
     *
     * Updates durations of any scopes at same or deeper level that go through
     * `newScope.startTime` to end at `newScope.startTime`.
     *
     * Params:
     * newScope = Scope to add. Must be open.
     * endOnly  = If true, do not add the scope - only use it to end previous scopes.
     *            Used by 'end' timepoint events.
     *
     * Returns: true if the scope has been inserted or is an end scope, false if it already exists.
     */
    bool scopeStart( in Scope newScope, bool endOnly )
    {
        assert( newScope.open, "A scope that's just been started cannot be closed" );

        return streams[newScope.stream].scopeStart( newScope, endOnly );
    }
}

private:

/// Array of tracks in a single stream.
alias TrackArray  = Track[];
/// Array of streams in ScopeManager.
alias StreamArray = Stream[];

/** Stores scopes for a single nest level of a stream.
 *
 * Scopes are ordered by time, each scope starting at or after where previous
 * ends.
 */
class Track
{
private:
    import std.range;

    // TODO Better data structure: Start w/ 1 array and split whenever we'd need
    // a move so we're always adding to the end of an array. Should work well
    // with data forgetting. Also split when current array gets too big (e.g.
    // 1M items). endScopeAt / addOpenScope will need to deal with a segmented
    // array: should be easy.

    // GC support disabled as EMSI containers seem to have issues with that
    // (TODO consider std.container, rcarray from mir or even rolling my own
    //  general (struct/class) array type)
    /// Container type used to store scopes.
    alias Scopes = DynamicArray!(Scope, IAllocator, false);

    /// Scopes of the track, ordered by time.
    Scopes scopes;

    /// Construct a Track using specified allocator for scope memory.
    this( IAllocator allocator )
    {
        scopes = Scopes( allocator );
    }

    /// Are there no scopes in the track?
    bool empty() const { return scopes.empty; }

    /** If there is a scope that intersects with newScope from the past,
     * shorten it so it doesn't intersect anymore.
     *
     * If there is a scope that intersects with newScope from the future,
     * shorten newScope.
     */
    void endScopeAt( in Scope newScope )
    {
        // Fast path: newScope is at the end of the track - cut the last scope
        // (which likely stretches to infinity)
        if( !scopes.empty && scopes.back < newScope )
        {
            scopes.back.end = min( scopes.back.end, newScope.start );
            return;
        }
        // Slow path: newScope is in the middle of the track - may need to cut
        // the last scope starting before newScope.
        auto lower = scopes[].assumeSorted.lowerBound( newScope );
        if( !lower.empty )
        {
            // If nearest previous scope ends after this new scope begins, cut it.
            lower.back.end = min( lower.back.end, newScope.start );
        }
    }

    /** Get the first scope (if any) starting at the same time as or after startScope.start.
     *
     * Returns: scope starting at or after startScope.start if there is any, null otherwise.
     */
    const(Scope)* nextScopeStartingFrom( in Scope startScope ) const
    {
        // Fast path: startScope is at/after the end of the track.
        if( !scopes.empty && scopes[].back < startScope)
        {
            return null;
        }
        // Slow path: startScope is somewhere in the middle of the track - 
        // return the first scope starting at or after startScope.start
        auto upper = scopes[].assumeSorted.upperBound( startScope );
        return upper.empty ? null : &(upper[0]);
    }

    /** Get the last scope (if any) starting at the same time as or before startScope.start.
     *
     * Returns: scope starting at or before startScope.start if there is any, null otherwise.
     */
    const(Scope)* lastScopeStartingUntil( in Scope startScope ) const
    {
        // Fast path: last scope starts at/before and ends at/after startScope.
        if( !scopes.empty && scopes[].back.start <= startScope.start && scopes[].back.end >= startScope.end)
        {
            return &(scopes[].back);
        }
        // Slow path: the last scope starting until startScope is somewhere in the middle.
        auto lower = scopes[].assumeSorted.lowerBound( startScope );
        return lower.empty ? null : &(lower[$-1]);
    }

    /** Get the last scope, if any.
     *
     * Returns: last scope in the track, or null if the track is empty.
     */
    const(Scope)* lastScope() const
    {
        return scopes.empty ? null : &(scopes[]).back;
    }

    /** Add a scope to the track.
     *
     * Params:
     * newScope = scope to add. It is assumed that no scope intersects with this
     *            scope (unless we already have this exact scope) - `endScopeAt`
     *            is used to ensure this.
     */
    void addNewScope( in Scope newScope )
    {
        // Easy case - new scope at the end.
        if( scopes.empty || scopes.back.end <= newScope.start )
        {
            scopes.insert( newScope );
            return;
        }

        // Adding scope in the middle of the track.
        auto lower = scopes[].assumeSorted.lowerBound( newScope );
        const insertPos = lower.length;
        assert( insertPos < scopes.length - 1, 
            "Due to scopes' sortedness, the case of scope at end should be handled in code above" );
        assert( lower.empty || lower.back.end <= newScope.start,
            "endScopeAt must be called to ensure previous scope does not intersect with the new one" );

        scopes.insert( newScope );
        // Move everything to make space for the new item at insertPos.
        // This will overwrite newly inserted item so we need to insert it again.
        // (the above insert makes space for the new item)
        moveAll( scopes[insertPos .. $ - 1], scopes[insertPos + 1 .. $] );
        scopes[insertPos] = newScope;
    }

    /** Copy scopes larger than given resolution intersecting with given time window to a buffer.
     *
     * Returns: slice of memoryToUse containing all written scopes.
     *
     * See_Also: ScopeManager.trackView
     */
    Scope[] copyView(
        in Time startTime,
        in Time endTime,
        in Time timeResolution,
        Scope[] memoryToUse ) const
    {
        if( scopes.empty )
        {
            return memoryToUse[0 .. 0];
        }
        Scope startDummy;
        startDummy.start = startTime;
        auto lower = scopes[].assumeSorted.lowerBound( startDummy );
        // Index of the scope starting right *before* the view, if any
        const size_t firstItemIndex = max( lower.length, 1 ) - 1; 

        Scope endDummy;
        endDummy.start    = endTime;
        endDummy.duration = Time(0);
        // Since scopes in track are consecutive and sorted by start, the are also
        // sorted by end. First item of upper starts *above* endTime so it's not in the view.
        auto upper = scopes[].assumeSorted.upperBound( endDummy );
        // Index of the last scope starting *before or at* the view end. 
        // The max() is necessary for when all scopes are after the view.
        const size_t lastItemIndex = max(scopes.length - upper.length, 1) - 1;

        size_t itemsUsed = 0;
        import std.typecons;
        Nullable!Scope mergedScope;

        // Writes scope to output.
        void addScope( ref const(Scope) _scope )
        {
            if( itemsUsed < memoryToUse.length ) { memoryToUse[itemsUsed++] = _scope; }
        }
        // Write merged scope, if it's large enough, into output and destroy it.
        void finishMergedScope()
        {
            if( !mergedScope.isNull && mergedScope.get().duration >= timeResolution )
            {
                addScope( mergedScope.get() );
                destroy( mergedScope );
            }
        }
        // Copy scopes within the range that are at least as large as resolution.
        // Also merge any consecutive scopes smaller than resolution, and copy
        // such merged scopes if they are large enough.
        foreach(ref _scope; scopes[firstItemIndex .. lastItemIndex + 1] )
        {
            if( _scope.duration >= timeResolution )
            {
                // If preceding scopes below resolution were merged together, add that to output.
                finishMergedScope();
                // Append current scope.
                addScope( _scope );
            }
            // Scope below resolution - start a merged scope.
            else if( mergedScope.isNull )
            {
                mergedScope = initMergedScope( _scope );
            }
            // A merged scope already exists and the new scope starts right after it - merge them.
            else if( _scope.start == mergedScope.get().end )
            {
                mergedScope.get().duration = _scope.end - mergedScope.get().start;
            }
            // A merged scope already exists but there is a gap before the new scope - do not merge.
            else
            {
                finishMergedScope();
                mergedScope = initMergedScope( _scope );
            }
        }
        // If preceding scopes below resolution were merged together, add that to output.
        finishMergedScope();

        return memoryToUse[0 .. itemsUsed];

        // TODO improve ignored scope merging:
        // - Consider merging scopes with gaps in between - name should also
        ///  contain info like '75% of time covered' since some time will be in gaps.
        // - If multiple scopes with same ID and they add up to more then TimeResolution,
        //   finish that merged scope as soon as we'd have to merge a scope with different ID,
        //   and generate name based on that ID's name.
        // - if above is not possible (need to merge scopes w/ different IDs to get above resolution),
        //   generate a name that includes info about the merged scopes.
        //   such as 'X calls of Y, Z calls of W, etc.'
    }

private:
    /** Initialize a merged scope starting with first scope to merge into it.
     */
    Scope initMergedScope( in Scope base) const
    {
        Scope result;
        // for RestartPoint.
        import timedilator.datasource;
        with( result )
        {
            name         = "merged";
            start        = base.start;
            duration     = base.duration;
            restartPoint = RestartPoint.init;
            stream       = base.stream;
            id           = TimepointIDMerged;
            nestLevel    = base.nestLevel;
        }
        return result;
    }
}

/** Nanoprof stream- usually scope data from a single thread of profiled process.
 *
 * Organized into Tracks - one per nest level.
 */
class Stream
{
private:
    /// Track used for internal profiling (nest level -128).
    Track internalTrack;

    /// Tracks used for user nestlevels - from -127 to +127.
    TrackArray nestLevelTracks;

    /// Start time of the newest scope in the track.
    Time lastUpdateTime_ = Time( 0 );

    /// Start time of the oldest scope in the track.
    Time firstUpdateTime_ = Time.Infinity;

public:
    /** Construct a Stream using specified allocator to allocate scopes.
     */
    this( IAllocator allocator )
    {
        internalTrack = new Track( allocator );
        foreach( i; 0 .. (NestLevel.max - NestLevel.min) )
        {
            nestLevelTracks ~= new Track( allocator );
        }
    }

    /** Start a new open scope and end any previous scopes at this or deeper nest levels.
     *
     * Shortens durations of any scopes at same or deeper level that go through
     * `newScope.startTime` to end at `newScope.startTime`.
     *
     * See_Also: ScopeManager.scopeStart
     *
     * Params:
     * newScope = Scope to add. Must be open. May be closed during adding if
     *            there are any scopes starting later on the same or higher nest
     *            levels.
     * endOnly  = If true, do not add the scope - only use it to end previous scopes.
     *            Used by 'end' timepoint events.
     *
     * Returns: true if the scope has been inserted or is an end scope, false if it already exists.
     */
    bool scopeStart( Scope newScope, bool endOnly )
    {
        const level = newScope.nestLevel;

        // We check if we already have the added scope so we don't overwrite
        // or duplicate scopes when re-reading. Only checking 'start' because
        // 'end' may vary depending on whether the scopes have been cut off by
        // parents/folowwing scopes.
        static bool duplicate( in Scope a, in Scope b ) { return a.id == b.id && a.start == b.start; }

        // Even if the scope is already known, previous scopes may be recently
        // readded and still open so we always call endScopeAt.

        // If this scope is on the nest level used for internal profiling, it
        // only closes scopes on that level.
        if( level == NestLevel.min )
        {
            trackAtLevel( level ).endScopeAt( newScope );
            auto nextScope = trackAtLevel( level ).nextScopeStartingFrom( newScope );
            if( nextScope != null )
            {
                if( duplicate( *nextScope, newScope ) )
                {
                    return false;
                }
                newScope.end = nextScope.start;
            }
        }
        else 
        {
            // Find previous scope on current and deeper levels, and if such scope
            // goes over start time, end it.
            foreach( NestLevel l; level .. NestLevel.max )
            {
                trackAtLevel( l ).endScopeAt( newScope );
            }

            // Find next scope on current level, if any, and shorten the newScope to end at nextScope.start
            auto nextScope = trackAtLevel( level ).nextScopeStartingFrom( newScope );
            if( nextScope != null )
            {
                // If nextScope starts at same time as newScope and has same ID, we already
                // have this scope and don't need to re-add it.
                if( duplicate( *nextScope, newScope ) )
                {
                    return false;
                }
                newScope.end = min( newScope.end, nextScope.start );
            }
            // Find parent scope, if any, and shorten the newScope to end at parentScope.end
            auto parentScope = findParentScope( newScope );
            if( parentScope != null )
            {
                newScope.end = min( newScope.end, parentScope.end );
            }
        }

        lastUpdateTime_  = max( lastUpdateTime_, newScope.start );
        firstUpdateTime_ = min( firstUpdateTime_, newScope.start );

        if( !endOnly )
        {
            // adding new scope
            trackAtLevel( level ).addNewScope( newScope );
        }
        return true;
    }

    /** Get the highest Time value of all scopes added so far.
     *
     * See_Also: ScopeManager.lastUpdateTime
     */
    Time lastUpdateTime() const { return lastUpdateTime_; }

    /** Get the lowest Time value of all scopes added to the stream.
     *
     * See_Also: ScopeManager.startTime
     */
    Time startTime() const { return firstUpdateTime_; }

    /** Get the track at specified level.
     *
     * Translates NestLevel, which is signed, to nestLevelTracks array index,
     * and handles the internal profiling tracks separately.
     */
    ref Track trackAtLevel( in NestLevel level )
    {
        if( level == NestLevel.min ) { return internalTrack; }
        return nestLevelTracks[level - NestLevel.min - 1];
    }

    /** Get read-only reference to track at specified level.
     *
     * See_Also: non-const trackAtLevel()
     */
    ref const(Track) trackAtLevel( in NestLevel level ) const
    {
        if( level == NestLevel.min ) { return internalTrack; }
        return nestLevelTracks[level - NestLevel.min - 1];
    }

private:
    /** Find the parent scope of child. if any.
     *
     * Note that there may be empty tracks between the current scope and its
     * parent - we skip those.
     *
     * Parent scope is defined as a scope that starts at most at the time the
     * child starts, and ends at least at the time the child starts.
     * The parent may end *during* the child because the child
     * may not yet be cut off to end at its parent's end.
     *
     * Returns: parent scope if any, null otherwise.
     */
    const(Scope)* findParentScope( in Scope child ) const
    {
        // The + 1 avoids the internal profiling track.
        foreach_reverse( NestLevel l; NestLevel.min + 1 .. child.nestLevel )
        {
            if( trackAtLevel( l ).empty )
            {
                continue;
            }
            // If this is null, we have no parent even though the track
            // has data (which should be invalid, but we could get such inputs I guess).
            auto candidate = trackAtLevel( l ).lastScopeStartingUntil( child );
            // If the last scope starting before child also ends before child, 
            // it's not a parent.
            return candidate.end >= child.start ? candidate : null;
        }
        return null;
    }
}
