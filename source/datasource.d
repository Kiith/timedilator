// Copyright (C) 2020 Ferdinand Majerech
// 
// This file is part of Timedilator.
// 
// Timedilator is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Timedilator is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Timedilator.  If not, see <http://www.gnu.org/licenses/>.

module timedilator.datasource;

import timedilator.error;
import timedilator.nanoprof;
import timedilator._scope;
import timedilator.scopemanager;

import std.array;
import std.algorithm;
import std.stdio;
import std.typecons;

public:

/** Point in nanoprof stream we can safely start reading events at.
 *
 * Used in Scopes for re-reading child scopes that have been forgotten to
 * save memory.
 */
struct RestartPoint
{
    /// Null RestartPoint, used e.g. in merged scopes (which are generated, not read).
    bool isNull() const
    {
        return offset == StreamOffset.max;
    }

private:
    /// File offset where we should start to read.
    StreamOffset offset = StreamOffset.max;
}

/** Data source reading a single nanoprof stream (e.g. from a single thread of a profiled process).
 *
 * Supports re-reading from a `RestartPoint` to re-read data we've forgotten to save memory.
 *
 * Actual input is done by DataReader objects - each of which can only read
 * forward, but separate DataReaders may be constructed to re-read data.
 */
abstract class DataSource
{
private:
    /** Reader used to read new data.
     *
     * This reader lives as long as the DataSource and always moves forward,
     * reading new data written by the profiled program.
     */
    DataReader forwardReader;

    /** Tracks names and nest levels associated to timepoint ID's, and how these
     * change in different parts of the stream due to ID re-registrations.
     *
     * This requires the registry to be common for all readers used by a
     * DataSource, so it cannot be a member of DataReader.
     */
    TimepointRegistry registry;

    /// ID of the stream being read by this DataSource.
    StreamID stream_;

    /// If we get an error, it is written here.
    TUIError error_;

    /// Various statistics about the stream being read.
    Stats stats_;

public:
    /// Various statistics about the stream being read.
    struct Stats
    {
        /// Number of events we've read so far, not including re-reading forgotten events.
        ulong eventsRead   = 0;
        /// Number of re-read events so far.
        ulong eventsReread = 0;
    }

    /** Construct a DataSource for stream with specified ID.
     *
     * Throws: Exception on error to open a file or another resource we're reading from.
     */
    this( in StreamID stream )
    {
        stream_       = stream;
        registry      = new TimepointRegistry();
        forwardReader = newReader( 0 );
    }

    /** Read more scope data from the source.
     *
     * Params:
     * scopeManager = Scope manager to load the read scopes to.
     *
     * Returns: true on success, false if we failed to read from the file or if
     *          the read data was not in valid nanoprof format. Use `error()` to
     *          get detailed error information.
     */
    final bool readForward( ScopeManager scopeManager )
    {
        scope( exit ) { error_ = forwardReader.error; }
        const result = forwardReader.readMore( scopeManager );
        stats_.eventsRead = forwardReader.stats.eventsRead;
        return result;
    }

    /** Re-read data that's previously been read. (for re-reading forgotten data)
     *
     * Params:
     * start        = Position to start reading at. This is usually
     *                Scope.restartPoint of the parent scope of any forgotten scopes.
     * end          = Position to stop reading at. Usually the Scope.restartPoint of
     *                the next scope after the parent of any forgotten scopes.
     * scopeManager = Scope manager to load the read scopes to. (Any already loaded
     *                scopes will be ignored).
     *
     * Returns: true on success, false if we failed to read from the file or if
     *          the read data was not in valid nanoprof format. Use `error()` to
     *          get detailed error information.
     */
    final bool rereadUpTo( const RestartPoint start, const RestartPoint end, ScopeManager scopeManager )
    in
    {
        assert( !start.isNull && !end.isNull,
            "Cannot read with null restartpoints (did you try to reread merged scopes?)" );
        assert( canReread,
            "Trying to reread with a data source that does not support rereading" );
        assert( start.offset < forwardReader.maxReadOffset(), 
            "reread restartpoint must be a point in file we've already read" );
    }
    do
    {
        auto reader = newReader( start.offset );
        assert( reader.nextEventType() == EventType.HighPrec,
            "reread restartpoint must point to a highprec event" );
        scope( exit ) { error_ = forwardReader.error; }
        scope( exit ) { stats_.eventsReread += reader.stats.eventsRead; }
        while( reader.maxReadOffset() < end.offset )
        {
            if( !reader.readMore( scopeManager ) )
            {
                return false;
            }
        }
        return true;
    }

    /// Get stats about the stream being read.
    ref const(Stats) stats() const { return stats_; }

    /** Can the DataSource implementation re-read forgotten data?
     *
     * Some implementations may not be able to re-read data, e.g. network-based
     * implementations.
     */
    bool canReread() const;

    /// Get ID of the stream we're reading.
    StreamID stream() const { return stream_; }

    /// Returns true if there have been no errors so far, false otherwise.
    bool ok() const { return error_.ok; }

    /// If ok() returns false, this returns funn error informations.
    TUIError error() const { return error_; }

protected:
    /** Constructs the DataReader implementation - which actually handles file reading.
     * 
     * Params:
     * startOffset = Offset to start reading at (can be non-zero for re-reading).
     */
    DataReader newReader( const StreamOffset startOffset );

    /// Get access to the timepoint registry to determine names/nestlevels of timepoint IDs.
    TimepointRegistry idMeta() { return registry; }
}

/** DataSource implementation reading a nanoprof stream from a file.
 *
 * Can read the file both once it's finished and in real-time as it's being written.
 */
class DataSourceFile: DataSource
{
private:
    /// Opens a new file descriptor on the file.
    File delegate() openFile;

public:
    /** Construct a DataSourceFile for stream with specified ID.
     *
     * Params:
     * rhsOpenFile = Delegate that will open the file to read the stream from.
     *               May be called multiple times to re-read forgotten data.
     *               This is expected to open the same file for a separate file
     *               descriptor.
     * stream      = ID of the stream we're reading.
     *
     * Throws: Any exception thrown by rhsOpenFile (e.g. if we fail to open the file).
     */
    this( File delegate() rhsOpenFile, in StreamID stream )
    {
        openFile = rhsOpenFile;
        super( stream );
    }

    override bool canReread() const { return true; }

protected:
    override DataReader newReader( const StreamOffset startOffset )
    {
        File file = openFile();
        return new DataReaderFile( super.stream, file, startOffset, super.idMeta );
    }
}

private:

/// Offset in a nanoprof stream, in bytes from start.
alias StreamOffset = ulong;

/// Offset value used for default-initialization and to signify end of a stream.
enum FileOffsetEOF = ulong.max;

/** Stores a nanoprof event with various metadata added (currently just the event's offset).
 */
struct ExtendedEvent(Event)
{
    /// Stored event.
    Event event;
    alias event this;
    /// Position in the stream the event was read from.
    StreamOffset offset;
}

/// Construct an ExtendedEvent grouping an event with the offset it was read at.
auto extendedEvent(Event)( in Event event, in StreamOffset offset )
{
    return ExtendedEvent!Event( event, offset );
}

/** Information about a registered timepoint ID.
 *
 * This information is valid at stream offset starting at startOffset and ending
 * at startOffset of the next TimepointMetadata with the same ID.
 */
struct TimepointMetadata
{
    /// Name of the scope started by the timepoint.
    string name;
    /// Nesting/'callgraph' level of the timepoint.
    byte level;
    /// Offset of the event in stream to determine which timepoint registration event applies to it.
    StreamOffset startOffset;
    /// Timepoint ID described by this metadata.
    TimepointID id;
}

/** Timepoint IDs and their corresponding names and nest levels are tracked here.
*
* Keeps track of even timepoint re-registrations, where a timepoint ID may
* have one name/nestlevel in one part of the stream and different
* name/nestlevel in another part.
*
* While scope data may be forgotten, timepoint registrations are not, so
* even if we re-read a timepoint with an ID re-registered later in stream,
* we will get the correct name and nestlevel that ID had in its position
* in the stream.
*
* Also holds all registered timepoint names, preventing them from being GC-collected.
* (we could even manually allocate the names and manage them here).
*/
class TimepointRegistry
{
private:
    /// Array of timepoint metadata sorted by startOffset.
    alias TimepointMetaArray = TimepointMetadata[];

    /** Timepoint metadata arrays for each timepoint ID - indexed by ID.
     *
     * Arrays for IDs that have never been registered are null.
     */
    TimepointMetaArray[] metadataByID;

public:
    /** Construct a TimepointRegistry, with no reigstered data yet.
     */
    this()
    {
        metadataByID = new TimepointMetaArray[NANOPROF_TIMEPOINT_ID_MAX + 1];
    }

    /// Get metadata for timepoint with specified ID found at specified offset in the stream.
    TimepointMetadata metadataFor( const TimepointID id, const StreamOffset timepointOffset ) const
    in
    {
        assert( timepointOffset != FileOffsetEOF, "Uninitialized offset?" );
        assert( id <= NANOPROF_TIMEPOINT_ID_MAX, "Timepoint ID out of bounds" );
    }
    do
    {
        auto metaArray = metadataByID[id];

        if( metaArray.empty )
        {
            return TimepointMetadata( "Unrecognized event ID - did you register it first?", -128, 0, id );
        }
        const firstRegistrationAfter = metaArray.countUntil!(a => a.startOffset >= timepointOffset );
        // If the first registration was done after this event, the event is not yet known at its point in stream.
        if( firstRegistrationAfter == 0 )
        {
            return TimepointMetadata( "Unrecognized event ID, but ID registered *after* event with this ID ", -128, 0, id );
        }
        // Return the newest / last metadata *before* timepointOffset (that is metaArray[firstRegistrationAfter - 1]).
        // If there was no registration *after* timepointOffset, that will be the last item in metaArray.
        return firstRegistrationAfter == -1 ? metaArray.back : metaArray[firstRegistrationAfter - 1];
    }

    /** Record a timepoint registration at specified offset.
     *
     * Params:
     * startOffset = Offset of the ID register event in source stream.
     *               If registerTimepointID is called multiple times with the same offset,
     *               only the first call will have an effect (we assume existing stream
     *               data does not change once it's written).
     * idEvent     = The ID register event, includes registered ID and its nest level.
     * name        = Name of the event ID.
     */
    void registerTimepointID( in StreamOffset startOffset, in IDRegisterEvent idEvent, in string name )
    in
    {
        assert( startOffset != FileOffsetEOF );
        assert( name != null, "a timepoint ID must have a valid name" );
        assert( idEvent.id <= NANOPROF_TIMEPOINT_ID_MAX, "Timepoint ID out of bounds" );
    }
    do
    {
        stderr.writefln( "registerTimepointID %s %s %s", startOffset, idEvent, name );
        const id       = idEvent.id;
        auto metaArray = metadataByID[id];

        const insertPos = metaArray.countUntil!(a => a.startOffset >= startOffset );
        // First registration of this ID (most common case).
        if( insertPos == -1 )
        {
            metadataByID[id] ~= TimepointMetadata( name, idEvent.level, startOffset, idEvent.id );
            return;
        }
        // This registration event is already known, ignore it.
        if( metaArray[insertPos].startOffset == startOffset )
        {
            return;
        }

        // Re-registration of an ID.
        metadataByID[id].insertInPlace(
            insertPos, TimepointMetadata( name, idEvent.level, startOffset, idEvent.id ) );
    }
}

/** Reads a single nanoprof stream in a linear fashion - can only move forward.
 */
abstract class DataReader
{
protected:
    /// If any error occurs, it is stored here.
    TUIError error_;

    /// Various statistics about the stream being read.
    Stats stats_;
public:
    /// Various statistics about the stream being read.
    struct Stats
    {
        /// Number of events read by this reader so far.
        ulong eventsRead = 0;
    }

    /** Read more data.
     *
     * Reads up to one 'range' of timepoint events between two highprec events.
     * (May read less if that data is not yet available in the file).
     *
     * Returns: true on success, false on an I/O error or on invalid data.
     */
    bool readMore( ScopeManager scopeManager );

    /** Get type of the next event in the stream.
     *
     * Returns:
     * * EventType of the next event on success.
     * * EventType.Unknown if there is an unknown/invalid event. In that case see `error()`.
     * * null if there is no event/we're at the end of the file.
     *
     * Throws: ErrnoException if there is an error reading from file.
     */
    Nullable!EventType nextEventType();

    /// Get the last error, if any, that has occured.
    TUIError error() const { return error_; }

    /// Get statistics about the stream being read.
    ref const(Stats) stats() const { return stats_; }

    /// Get the maximum stream offset reached so far (not including any internal buffering).
    StreamOffset maxReadOffset() const;
}

/** DataReader implementation reading nanoprof stream from a file.
 *
 * Can read from the file as it is being written.
 */
class DataReaderFile: DataReader
{
private:
    import std.exception;
    import std.conv;
    import std.format;

    /// Base size for the read buffer (the buffer may get larger).
    enum BASE_BUFFER_SIZE = 64 * 1024;

    /// ID of the stream we're reading.
    StreamID stream;

    /// File we're reading from.
    File file;

    /// Current offset in the file/stream.
    StreamOffset currentOffset = 0;

    /// Buffers data read from the file, so we can work on slices of it.
    ubyte[] buffer;

    /** Position of the first byte not yet 'consumed' by `get()` in the buffer.
     *
     * `peek()` and `get()` both access buffer data from this point on, and
     * `get()` also moves this position.
     */
    size_t bufferPosition = 0;

    /// Get the number of buffered bytes not yet consumed by `get()`.
    size_t bufferedBytes() const { return buffer.length - bufferPosition; }

    /** An IDRegisterEvent is immediately followed by a string data event with
     * the name of the registered timepoint, so we save the IDRegisterEvent here
     * until we process the next event.
     */
    Nullable!(ExtendedEvent!IDRegisterEvent) pendingRegisterEvent;

    /** A HighPrec event needs the next Timepoint event to know the full extent
     * of 'timepoint time' between 2 highprec events. The timepoint may not
     * necessarily follow the highprec event *immediately*.
     *
     * Note that there may be highprec events with no following timepoints, e.g.
     * when a buffer is flushed due to running out of space for register events
     * (as opposed to timepoint events). In this case, pendingHighPrec just gets
     * rewritten without being used, and a timepoint range is not ended.
     */
    Nullable!(ExtendedEvent!HighPrecEvent) pendingHighPrecEvent;

    /** Previous HighPrec event, denoting the start of current 'timepoint range'.
     *
     * Timepoints are buffered between HighPrec events, as the latter are needed
     * to calibrate the 'clock rate' of the timepoint events in between.
     */
    Nullable!(ExtendedEvent!HighPrecEvent) timepointRangeStart;

    /** Parsed timepoint events are buffered here until we reach a HighPrec event
     * - at that point the events are processed and the buffer is cleared.
     */
    ExtendedEvent!TimepointEvent[] timepointBuffer;

    /** Timepoint registry, to register and access metadata about registered timepoints
     * in various parts (offsets) of the file/stream.
     */
    TimepointRegistry idMeta;

public:
    invariant
    {
        /// bufferPosition must always point into buffer or to its end.
        assert( bufferPosition <= buffer.length,
                "bufferPosition out of bounds (%s > %s)".format( bufferPosition, buffer.length ) );
    }

    /** Construct a DataReaderFile.
     *
     * Params:
     * rhsStream   = ID of the stream we're reading.
     * rhsFile     = File to read to. Is moved into the DataReaderFile.
     * startOffset = Start offset to read at in the file. Must point to file
     *               start or an offset retrieved from a RestartPoint.
     * registry    = Timepoint registry to register and access timepoint metadata.
     *
     * Errors:
     * If we're unable to seek to startOffset. Read `error()` to check for this.
     */
    this( in StreamID rhsStream, File rhsFile, const StreamOffset startOffset, TimepointRegistry registry )
    in
    {
        assert( startOffset != FileOffsetEOF );
    }
    do
    {
        stream        = rhsStream;
        file          = rhsFile;
        currentOffset = startOffset;
        buffer.reserve( BASE_BUFFER_SIZE );
        assert( registry !is null, "Null timepoint registry passed to DataReaderFile" );
        idMeta        = registry;
        try
        {
            file.seek( startOffset, SEEK_SET );
        }
        catch( Exception e )
        {
            const guess = startOffset == 0
                ? "Probably when opening the file for the first time?"
                : "Probably re-reading swapped-out data?";
            readError( "failed to seek to read from position %s (%s).".format( startOffset, guess ),
                       e.to!string,
                       "profiled process should be writing this file - check that",
                       "check file permissions" );
        }
    }

    override bool readMore( ScopeManager scopeManager )
    {
        // No buffered data and we the file has reached EOF (but may have been written since).
        if( noData )
        {
            // Error indicator set - give up.
            if( file.error() )
            {
                readError( "file error (ferror()) indicator set",
                           "trying to return to reading after previously reaching end of profiling data." );
                return false;
            }
            // Clears error and EOF indicator.
            file.clearerr();
        }
        // If we've had an error, don't try to continue reading.
        if( !error_.ok )
        {
            return false;
        }

        // Read nanoprof stream events in a loop.
        bool done = false;
        StreamOffset eventOffset;
        try while( !done )
        {
            // 4 things may happen here:
            // - EOF - done for now but there may yet be more data written in future.
            //   (we don't detect that 'the app is dead' and that is probably OK)
            // - file access error - exception is thrown
            // - unparsable data - returned through type Unknown
            // - parsable data - valid type

            // Offset of the next event in the file.
            eventOffset = currentOffset - bufferedBytes();
            // Can throw ErrnoException
            Nullable!EventType type = nextEventType();

            // EOF - done reading for now (more data may be written later).
            if( type.isNull ) { break; }
            // If type is unknown, error is set in nextEventType. If readEvent fails, it is set in readEvent.
            if( type.get() == EventType.Unknown ||
                // Can throw ErrnoException
                !readEvent( type.get(), eventOffset, scopeManager, done ) )
            {
                return false;
            }
        }
        catch( ErrnoException e )
        {
            readError(
                "Failed to read data somewhere after byte %s".format( eventOffset ),
                e.to!string,
                "profiled process should be writing this file - check that",
                "check file permissions" );
            return false;
        }
        return true;
    }

    override Nullable!EventType nextEventType()
    {
        if( ubyte[] bytes = peek( 1 ) )
        {
            auto type = bytes[0].toEventType();
            // Write as much context as possible if we can't parse an event
            // (probably a nanoprof writing or data source parsing bug).
            if( type == EventType.Unknown )
            {
                import std.string;
                auto to_hex(in ubyte[] data) { return data.map!((b) => "%x".format(b) ).join; }
                string byteContext = 
                    to_hex( buffer[max(0, cast(int)(bufferPosition) - 4) .. bufferPosition] ) ~
                    "->" ~
                    to_hex( peek(5) );
                formatError(
                    "invalid unparsable data", bufferPosition,
                    "expected a nanoprof event to start here, got byte after '->' here: %s".format( byteContext ),
                    "maybe the file got corrupted - is the profiled program running correctly?",
                    "maybe the nanoprof library has a bug and generated invalid data" );
            }
            return nullable( type );
        }
        // No more data; we've probably processed everything written by the process so far.
        assert( file.eof && bufferPosition == buffer.length,
            "returning no events but we sill have data to read" );
        return Nullable!EventType.init;
    }

    override StreamOffset maxReadOffset() const { return currentOffset; }

private:
    /** Set error_ to a StreamReadError.
     *
     * Params:
     * main    = What happened.
     * context = Error context / where it happened.
     * ideas   = Ideas about why it might have happened.
     */
    void readError( string main, string context, string[] ideas ... )
    {
        error_ = streamReadError(
            "Input file %s: %s\n Context: %s\n %s"
            .format( file.name, main, context, ideas ? "Ideas: " ~ ideas.join(", ") : "" ) );
    }

    /** Set error_ to a StreamFormatError.
     *
     * Params:
     * main    = What happened.
     * offset  = Position where it happened. size_t max if we don't have a position.
     * context = Error context / where it happened.
     * ideas   = Ideas about why it might have happened.
     */
    void formatError( string main, size_t offset, string context, string[] ideas ... )
    {
        const offset_str = offset == size_t.max ? "" : "@byte %u".format( offset );
        error_ = streamFormatError(
            "Input file %s %s: %s\n Context: %s\n %s"
            .format( file.name, offset_str, main, context, ideas ? "Ideas: " ~ ideas.join(", ") : "" ) );
    }

    /** Set error_ to a StreamFormatError, with no position information
     *
     * See_Also: formatError
     */
    void formatError( string main, string context, string[] ideas ... )
    {
        formatError( main, size_t.max, context, ideas );
    }

    /** Read event after parsing its type.
     *
     * Params:
     * type         = Type the event to read.
     * eventOffset  = Offset of the event in file.
     * scopeManager = Scope manager to add any finalized scopes to.
     * done         = Set to true when we should stop reading events for current
     *                readMore call (when we process a timepoint buffer).
     *
     * Returns: true on success< false on error (e.g. invalid data).
     *
     * Throws: ErrnoException on a file read error.
     */
    bool readEvent( in EventType type, in StreamOffset eventOffset, ScopeManager scopeManager, out bool done )
    {
        done = false;
        final switch( type ) with( EventType )
        {
            case Timepoint:
                // TODO Due to timepoint buffering we get scopes in bursts which
                // is jittery. Emit additional highprec events at any time when
                // immediate time delta is >1M cycles. Only use immediate delta
                // to avoid extra vars / logic in timepoint emit code. Do this
                // in timepoint slow path. Will not make dense streams any
                // better, but those don't need to be real-time .

                auto maybeEvent = readEventTimepoint();
                if( maybeEvent.isNull )
                {
                    formatError(
                        "unexpected EOF reading event starting at this byte", eventOffset,
                        "was reading a timepoint event, did not get all needed data",
                        "most likely a bug in datasource.d, we're reading a file being written, EOF in middle of an event could be expected" );
                    return false;
                }
                // Buffer timepoints until we reach a highprec event. Only then
                // can we determine the 'clock rate' for the timepoints.
                timepointBuffer ~= extendedEvent( maybeEvent.get(), eventOffset );

                // If we've just had a highprec event, use it to set timepoint
                // time and process timepoints since last highprec event.
                if( !pendingHighPrecEvent.isNull )
                {
                    const firstHighPrec = timepointRangeStart.isNull;
                    processTimepoints( pendingHighPrecEvent.get(), scopeManager );
                    destroy( pendingHighPrecEvent );

                    // readMore reads a 'timepoint range' between 2 highprec
                    // events, usually meaning 'read until next highprec event'
                    // - previous one is range start. but for the first call
                    // there's no previous highprec event, so continue reading.
                    if( !firstHighPrec )
                    {
                        done = true;
                    }
                }
                break;
            case HighPrec:
                const maybeHighPrec = readEventHighPrec();
                if( maybeHighPrec.isNull )
                {
                    formatError(
                        "unexpected EOF reading event starting at this byte", eventOffset,
                        "was reading a highprec event, did not get all needed data",
                        "most likely a bug in datasource.d, we're reading a file being written, EOF in middle of an event could be expected" );
                    return false;
                }
                // Need next timepoint to associate highprec w/ timepoint time.
                pendingHighPrecEvent = extendedEvent( maybeHighPrec.get(), eventOffset );
                break;
            case Data:
                // TODO TUI support for using/plotting data events
                //      and assigning them to scopes.
                const maybeEvent = readEventData();
                if( maybeEvent.isNull )
                {
                    // Error written in readEventData.
                    break;
                }

                const event = maybeEvent.get;
                // A timepoint ID register event is immediately followed by a string data
                // event storing the ID's name - handle that here.
                if( !pendingRegisterEvent.isNull )
                {
                    if( event.data.type() != typeid(string) )
                    {
                        formatError(
                            "unexpected event", eventOffset,
                            "expected a string data event after an ID register event, got a data event with type %s".format( event.data.type() ),
                            "most likely there is a bug in nanoprof.h" );
                        return false;
                    }
                    assert( idMeta !is null, "Null timepoint registry when reading events" );
                    idMeta.registerTimepointID(
                        pendingRegisterEvent.get.offset,
                        pendingRegisterEvent.get.event,
                        event.data.get!string );
                    destroy( pendingRegisterEvent );
                }
                break;
            case IDRegister:
                const maybeEvent = readEventIDRegister();
                if( maybeEvent.isNull )
                {
                    formatError(
                        "unexpected EOF reading event starting at this byte.", eventOffset,
                        "was reading an ID register event, did not get all needed data",
                        "most likely a bug in datasource.d, we're reading a file being written, EOF in middle of an event could be expected" );
                    return false;
                }
                if( !pendingRegisterEvent.isNull )
                {
                    formatError(
                        "unexpected event", eventOffset,
                        "expected a string data event after an ID register event, got another ID register event.",
                        "most likely there is a bug in nanoprof.h" );
                    return false;
                }
                // Store the event; will register the ID after parsing immediately
                // following string data event storing name to use for the ID.
                pendingRegisterEvent = extendedEvent( maybeEvent.get(), eventOffset );
                break;
            case Padding:
                skipPadding();
                break;
            case Unknown:
                assert( false, "handled by caller" );
        }
        ++stats_.eventsRead;
        return true;
    }

    /** Process buffered timepoints, a 'timepoint range' ending at the last highprec event.
     *
     * Params:
     * timepointRangeEnd = HighPrec event at the end of the timepoint range -
     *                     its time value sets time of last timepoint in buffer.
     * scopeManager      = Scope manager to write any scopes started in this range to.
     *
     * Note:
     * this.timepointRangeStart sets time of last timepoint in previous range;
     * first timepoint in this range starts at a delta after timepointRangeStart
     */
    void processTimepoints( in ExtendedEvent!HighPrecEvent timepointRangeEnd, ScopeManager scopeManager )
    {
        // Either at the beginning of file or the beginning or a reread.
        // The only thing we can do is save the timepoint range start.
        if( timepointRangeStart.isNull )
        {
            // Special case for first highprec+timepoint; do not delete first
            // timepoint. Any timepoints before that are fair game, as the first
            // timepoint must be preceded by a highprec event (which must be at
            // least at the beginning of each bufffer). Timepoint is inserted to
            // timepointBuffer before processTimepoints() is called with its
            // associated highprec event - so that timepoint is guaranteed to be
            // here.
            timepointBuffer = timepointBuffer[$ - 1 .. $];
            // First timepoint's delta *must* be zero, otherwise the first
            // timepoint would be at a time delta *relative* to its highprec
            // event. This is true for first timepoints in all timepoint ranges
            // *except* the first one: timepoints corresponding to those
            // ranges' end highprec events set the time of range end, not start,
            // and the first timepoint in next range is at delta relative to
            // that - but the very first timepoint is only used to know
            // 'clock time' value of the first timepoint range start, so it
            // cannot be at a delta relative to that start.
            timepointBuffer[0].timeDelta = 0;
            timepointRangeStart = timepointRangeEnd;
            return;
        }

        // Reset the timepoint buffer and save the start for the next timepoint range when done.
        scope(exit)
        {
            timepointBuffer     = timepointBuffer[0 .. 0];
            timepointRangeStart = timepointRangeEnd;
        }

        // When the program is registering timepoint metadata it may flush a
        // buffer with no timepoints. Each buffer starts with a highprec event,
        // so this may happen.
        if( timepointBuffer.empty )
        {
            return;
        }

        // Time between the highprec events.
        // Limiting precision issues by counting time in picoseconds.
        const ulong deltaPicosecs = 1000 * (timepointRangeEnd.nsecs - timepointRangeStart.get.nsecs);

        // Total 'time units' (e.g. RDTSC) between the HighPrec events.
        const ulong deltaCycles = timepointBuffer.map!( p => p.timeDelta ).sum;

        // One 'time unit' of the time delta.
        const ulong picoSecondsPerCycle = deltaPicosecs / deltaCycles;

        // Compute absolute time values of the individual events in timepointBuffer,
        // and add them to scopeManager as newly started scopes.
        ulong cumulativePicosecs = 1000 * timepointRangeStart.get().nsecs;
        foreach( ref timepoint; timepointBuffer )
        {
            cumulativePicosecs += timepoint.timeDelta * picoSecondsPerCycle;

            auto meta = idMeta.metadataFor( timepoint.id, timepoint.offset );
            Scope scopeStart;
            with( scopeStart )
            {
                id           = timepoint.id;
                name         = meta.name;
                start        = Time( cumulativePicosecs );
                // The scope is open (continues into infinity).
                duration     = Time.Infinity;
                restartPoint = RestartPoint( timepointRangeStart.get().offset );
                stream       = this.stream;
                nestLevel    = meta.level;
            }
            // This will add a new scope if it is not already present.
            scopeManager.scopeStart( scopeStart, timepoint.isEnd );
        }
    }

    /// True when we're at the end of data written by the process so far.
    bool noData() const
    {
        return file.eof && bufferedBytes == 0;
    }

    /** Read a timepoint event to `result`.
     *
     * A timepoint event can be in one of 3 formats described in FORMAT.md .
     *
     * Call iff `nextEventType == EventType.Timepoint`.
     *
     * Returns: event on success, null if we ran out of data.
     *
     * Throws: ErrnoException on a file read error.
     */
    Nullable!TimepointEvent readEventTimepoint()
    {
        assert( nextEventType == EventType.Timepoint, "unexpected event read function" );
        // If the event is in the 2B format, this stores the entire event.
        // For 4B/8B format, this stores the ID.
        const(ubyte)[] bytes = peek( 2 );
        if( bytes.empty ) { return Nullable!TimepointEvent(); }
        // If 1st bit of 2nd byte is 0, that byte is a 1B timedelta and
        // we have the 2B format.
        const bool format2B = 0 == (bytes[1] & 0b1000_0000);

        // Simplest format: 7-bit ID with a 7-bit time delta, top bits 0.
        if( format2B )
        {
            get( 2 );
            return nullable( TimepointEvent( bytes[0], bytes[1] ) );
        }

        bytes = peek( 4 );
        // Ran out of data
        if( bytes.empty ) { return Nullable!TimepointEvent(); }
        // If 1st bit of 3rd byte is 0, that byte is the start of a 2B
        // timedelta (4B format), otherwise the timedelta is 6B (8B format)
        const bool format4B = 0 == (bytes[2] & 0b1000_0000) ;
        // Compose a 14-bit ID from the 2 bytes (top bits indicate format).
        const id = (bytes[0] << 7) | (bytes[1] & 0b0111_1111);
        if( format4B )
        {
            get( 4 );
            return nullable( TimepointEvent( id, (bytes[2] << 8) | bytes[3] ) );
        }

        bytes = get( 8 );
        // Ran out of data
        if( bytes.empty() ) { return Nullable!TimepointEvent(); }
        // Top bit of bytesTime[0] is 1 so we have to start by zeroing that.
        const timeDelta = 
            ((bytes[2] & 0b0111_1111UL) << 40) |
            (cast(ulong)(bytes[3]) << 32) |
            (cast(ulong)(bytes[4]) << 24) |
            (cast(ulong)(bytes[5]) << 16) |
            (cast(ulong)(bytes[6]) << 8) |
             cast(ulong)(bytes[7]);
        return nullable( TimepointEvent( id, timeDelta ) );
    }

    /** Read a high-precision timestamp event.
     *
     * Records a precise timestamp in nanoseconds.
     *
     * Each highprec event is associated with the next timepoint event for which
     * it sets the time value. Each pair of successive highprec events is used
     * to compute time delta and clock rate between associated timepoint events.
     *
     * Format: see FORMAT.md
     *
     * Call iff `nextEventType == EventType.HighPrec`.
     *
     * Returns: event on success, null if we ran out of data.
     *
     * Throws: ErrnoException on a file read error.
     */
    Nullable!HighPrecEvent readEventHighPrec()
    {
        assert( nextEventType == EventType.HighPrec, "unexpected event read function" );
        if( ubyte[] bytes = get( 8 ) )
        {
            return nullable( HighPrecEvent(
                (cast(ulong)(bytes[1]) << 48) |
                (cast(ulong)(bytes[2]) << 40) | (cast(ulong)(bytes[3]) << 32) |
                (cast(ulong)(bytes[4]) << 24) | (cast(ulong)(bytes[5]) << 16) |
                (cast(ulong)(bytes[6]) <<  8) |  cast(ulong)(bytes[7]) ) );
        }
        return Nullable!HighPrecEvent();
    }

    /** Read a data event, which stores a single value of various datatypes.
     *
     * Used to record interesting tracing data, which may later be e.g. graphed.
     *
     * Format: See FORMAT.md
     *
     * Returns: event on success, null if we ran out of data or on unknown datatype.
     *
     * Throws: ErrnoException on a file read error.
     */
    Nullable!DataEvent readEventData()
    {
        assert( nextEventType == EventType.Data, "unexpected event read function" );
        const ubyte[] typeByte = peek(1);
        assert( typeByte.length == 1,
            "first byte must be available from nextEventType()" );

        // Read a simple value of specified type.
        Nullable!DataEvent readSimpleData(T)()
        {
            auto typeAndData = get( 1 + T.sizeof );
            if( !typeAndData )
            {
                formatError(
                    "unexpected EOF reading a data event.", 
                    "was reading a data event, did not get all needed data",
                    "most likely a bug in datasource.d, we're reading a file being written, EOF in middle of an event could be expected" );
                return Nullable!DataEvent();
            }
            T[] data = cast(T[])typeAndData[1 .. $];
            assert( data.length == 1, "On success, this get() must return one value" );
            return nullable( DataEvent( data[0] ) );
        }

        const type = typeByte[0] & 0xF;
        switch( typeByte[0] & 0xF )
        {
            // unknown data type
            case 0x0:
                formatError(
                    "unknown data event type %X.".format( type ),
                    "was reading a data event",
                    "maybe nanoprof.h has been updated with support for new types?" );
                return Nullable!DataEvent();
            case 0x1: return readSimpleData!ubyte;
            case 0x2: return readSimpleData!ushort;
            case 0x3: return readSimpleData!uint;
            case 0x4: return readSimpleData!ulong;
            case 0x5: return readSimpleData!float;
            case 0x6: return readSimpleData!double;
            // unknown data type
            case 0x7 - 0xE: return Nullable!DataEvent();
            // Special handling for the string type, which can be arbitrary-length.
            case 0xF:
                // Move to after the type byte
                get( 1 );
                // Read until, not including, a zero terminator byte.
                if( ubyte[] data = getUntil( 0 ) )
                {
                    auto result = nullable( DataEvent((cast(char[])data).idup) );
                    auto terminator = get(1);
                    // skip the zero terminator
                    assert( terminator == [0], "getUntil( 0 ) must end at a zero terminator" );
                    return result;
                }
                formatError(
                    "unexpected EOF reading string data event starting at this byte", 
                    "was reading a string, ran out of data before the string ended",
                    "maybe the terminating zero byte was not written by nanoprof.h (bug)?",
                    "most likely a bug in datasource.d, we're reading a file being written, EOF in middle of an event could be expected" );
                // Ran out of data before the first string byte.
                return Nullable!DataEvent();
            default: assert( false );
        }
        assert( false );
    }

    /** Read a timepoint ID registering event.
     *
     * Specifies the nest level and name for a timepoint ID.
     *
     * The name is in an immediately following data event with the string type.
     *
     * Format: see FORMAT.md
     *
     * Returns: event on success, null if we ran out of data.
     *
     * Throws: ErrnoException on a file read error.
     */
    Nullable!IDRegisterEvent readEventIDRegister()
    {
        assert( nextEventType == EventType.IDRegister, "unexpected event read function" );
        if( ubyte[] bytes = get( 4 ) )
        {
            const level = cast(byte)bytes[1];
            // 14-bit ID
            const id    = ((bytes[2] & 0b00111111) << 8) | bytes[3];
            return nullable( IDRegisterEvent( level, id ) );
        }
        return Nullable!IDRegisterEvent();
    }

    /// Skip a padding byte; call iff nextEventType == EventType.Padding.
    void skipPadding() nothrow
    {
        auto skipped = get( 1 ).assumeWontThrow;
        assert( skipped.length == 1 );
    }

    /** Consume the next `size` bytes.
     *
     * Returns:
     * Slice of `size` bytes on success, empty slice if we run out of data.
     *
     * Throws: ErrnoException on a file read error.
     */
    ubyte[] get( const size_t size )
    {
        scope(exit) { bufferPosition += size; }
        return peek( size );
    }

    /** Look at next `size` bytes in the stream without consuming them.
     *
     * Returns:
     * Slice of `size` bytes on success, empty slice if we run out of data.
     *
     * Throws: ErrnoException on a file read error.
     */
    ubyte[] peek( const size_t size )
    {
        // buffer position - if so, name it as such
        const needMoreData = bufferPosition + size > buffer.length;
        while( needMoreData && !file.eof )
        {
            bufferMoreData();
        }
        const inBounds = bufferPosition + size <= buffer.length;
        return inBounds ? buffer[bufferPosition .. bufferPosition + size] : [];
    }

    /** Consume all bytes until `terminator` is reached. `terminator` is also consumed.
     *
     * Used to read e.g. zero-terminated strings.
     *
     * TODO rewrite: EOF can get hit in the middle of a string being written non-atomically,
     *      so we need to be able to handle that.
     * Returns:
     * * Variable-size slice of bytes ending right before `terminator`.
     * * Empty slice if EOF is reached without encountering `terminator`.
     */
    ubyte[] getUntil( const ubyte terminator )
    {
        // Look for the terminator in buffered data.
        auto terminatorIndex = buffer[bufferPosition .. $].countUntil( terminator );
        // Continue buffering until we find the terminator.
        while( terminatorIndex <= 0 )
        {
            // EOF reached without finding terminator
            if( file.eof ) { return []; }
            bufferMoreData();
            // This re-reads buffer data from previous iterations, optimize if needed.
            terminatorIndex = buffer[bufferPosition .. $].countUntil( terminator );
        }
        return get( terminatorIndex );
    }

    /** Read more data from the file and add it to `buffer`.
     *
     * Throws away consumed data and resets bufferPosition to 0.
     *
     * Throws: ErrnoException on a file read error.
     */
    void bufferMoreData()
    {
        // Throw away data we've done processing.
        buffer[bufferPosition .. $].moveAll( buffer[0 .. $ - bufferPosition] );

        buffer = buffer[0 .. $ - bufferPosition];
        bufferPosition = 0;
        ubyte[BASE_BUFFER_SIZE] readBuf;
        auto readData = file.rawRead(readBuf);
        currentOffset += readData.length;
        buffer ~= readData;
    }
}

// TODO may get 'data cut off'/'data interrupted' errors for events read in
// multiple `get`s due to non-atomic file writes. E.g. we have type byte but not
// content of a data event.
// If this happens, rewrite the event reading functions to:
//   - on incomplete data: return no event but it will not be an error.
//     currently 'null' means 'unexpected EOF' - but here EOF will be expected
//   - not consume data before done (only string data events still do this!)
// TODO consider rewriting this into a proper state machine with states like:
//   - 'readEventID' // after an event
//   - 'readEventIDString' // right after an IDRegisterEvent
//   - 'readDataEventType'
//   - 'readDataEventSimpleData'
//   - 'readDataEventStringData'
// TODO if we do the above, split DataReader into separate DataReader (which
// provides consecutive data starting at an offset, and nothing else) and
// DataParser (which would be used regardless of where we get our data from)
// TODO DataReaderSocket (will not support reread)
// TODO DataReaderSystemVIPC (will not support reread)
// TODO DataReaderIOUring
// TODO maybe store data coming from a network data source, and use a file data
// reader for re-reading.
