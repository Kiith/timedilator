// Copyright (C) 2020 Ferdinand Majerech
//
// This file is part of Timedilator.
//
// Timedilator is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Timedilator is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Timedilator.  If not, see <http://www.gnu.org/licenses/>.

module timedilator.nanoprof;

/// Max value of any timepoint event ID (including internal IDs).
enum NANOPROF_TIMEPOINT_ID_MAX = 0x3FFF;
/// Max value of a user-specified timepoint event ID.
enum NANOPROF_TIMEPOINT_ID_MAX_USER = 0x3BFF;
/// Value of timepoint 'scope end' event for level -128
enum NANOPROF_TIMEPOINT_ID_MIN_END  = (NANOPROF_TIMEPOINT_ID_MAX_USER + 1);
/// Value of timepoint 'scope end' event for level 127
enum NANOPROF_TIMEPOINT_ID_MAX_END  = (NANOPROF_TIMEPOINT_ID_MIN_END + 255);

/** Timepoint event ID.
 *
 * While nanoprof uses 16bit values (at least at the moment), we use 32bit so
 * we can have special TUI-only timepoint ID's, e.g. for scopes created by
 * merging multiple smaller scopes.
 */
alias TimepointID = uint;

/** TUI-only timepoint ID for merged scopes.
 *
 * When we have a group of scopes too small to be displayed individually but
 * large enough to be displayed together, we merge them into a scope with this
 * ID (see Track.copyView).
 */
enum TimepointIDMerged = uint.max - 1;


/** Timepoint/scope nest level.
 * 
 * Scopes with higher nest levels nest inside scopes with lower nest levels,
 * except for -127 (NestLevel.min), which is used for internal profiling and
 * higher nest levels do not nest in it.
 */
alias NestLevel = byte;


/// Nanoprof event types.
enum EventType: uint
{
    /// Time point: time delta from last time point + event ID.
    Timepoint   = 0,
    /// High-precision timestamp. Used to correct time point values.
    HighPrec    = 0xFD,
    /// Data event: data type followed by an integer/float/string.
    Data        = 0xA,
    /// Timepoint event ID registration. Assigns a level and name to an ID.
    IDRegister  = 0xFE,
    /// Padding byte.
    Padding     = 0xFF,
    /// Unknown/error case.
    Unknown     = 0xFFFFFFFE,
}

/** Determine type of the current nanoprof event.
 *
 * Event type can always be determined from the first byte of an event.
 *
 * Event type is determined by a bit 'prefix' of the first byte of the event.
 * Different event types may have prefixes of different lengths. Remaining
 * bits (if any) after the prefix are used for event data. More common events
 * have shorter prefixes so they can use more bits for the event itself.
 *
 * Event type prefixes:
 * * 0b0:        timepoint (most common event)
 * * 0b1010:     data
 * * 0b11111101: highprec
 * * 0b11111110: timepoint event ID register
 * * 0b11111111: padding byte
 */
EventType toEventType( const ubyte firstByte )
{
    if( (firstByte >> 7) == 0b0 )    { return EventType.Timepoint; }
    if( (firstByte >> 4) == 0b1010 ) { return EventType.Data; }
    switch( firstByte )
    {
        case 0b11111101: return EventType.HighPrec;
        case 0b11111110: return EventType.IDRegister;
        case 0b11111111: return EventType.Padding;
        default: break;
    }

    import std.stdio;
    return EventType.Unknown;
}

/** Timepoint event data extracted from nanoprof stream.
 * 
 * The event only contains an ID + time delta in the stream; that ID and delta
 * is used to determine name, level and exact time after the event is read.
 * 
 * See: FORMAT.md for more information
 */
struct TimepointEvent
{
    /** ID of the timepoint event.
     *
     * Currently, timepoint event IDs are limited to 14-bit values.
     */
    TimepointID  id;

    /** Time delta from the last timepoint event, in 'time units'.
     *
     * 'Clock rate' of this time depends on the time measurement method, and is
     * calibrated by the high precision timestamp events. See FORMAT.md for more
     * information.
     */
    ulong timeDelta;

	/// True if this is an 'end' event and does not start a scope.
	bool isEnd() const
	{
		return id >= NANOPROF_TIMEPOINT_ID_MIN_END && id <= NANOPROF_TIMEPOINT_ID_MAX_END;
	}
}

/** High-precision timestamp event.
 *
 * Uses high-precision clock to measure time in nanoseconds, which is used to
 * set the 'exact time' value of immediately following TimepointEvent.
 */
struct HighPrecEvent
{
    /// Exact time in nanoseconds.
    ulong nsecs;
}

/// A data event, recording a tracing value/stat of one of various data types.
struct DataEvent
{
    import std.variant;
    /// Using Algebraic! to store the data as a union - should be fast enough.
    Algebraic!(
        ubyte,
        ushort,
        uint,
        ulong,
        float,
        double,
        string) data;

    /// Construct a data event from its data type.
    this(T)( T value )
    {
        data = value;
    }
}

/** Event registering a timepoint event ID to give a level and name.
 *
 * The name is stored in a data event immediately after the ID register event.
 * This data event must be parsed to get the ID's name.
 */
struct IDRegisterEvent
{
    /// Nest level of the event.
    byte level;
    /// ID being registered.
    TimepointID id;
}
