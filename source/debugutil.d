// Copyright (C) 2020 Ferdinand Majerech
//
// This file is part of Timedilator.
//
// Timedilator is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Timedilator is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Timedilator.  If not, see <http://www.gnu.org/licenses/>.

module timedilator.debugutil;

/// Mixin string to insert breakpoint. No ARM implementation at the moment.
version(X86)
{
    enum breakpoint = "asm { int 3; }";
}
version(X86_64)
{
    enum breakpoint = "asm { int 3; }";
}
else
{
    enum breakpoint = "";
}
