// Copyright (C) 2020 Ferdinand Majerech
//
// This file is part of Timedilator.
//
// Timedilator is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Timedilator is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Timedilator.  If not, see <http://www.gnu.org/licenses/>.

module timedilator.panes;

import std.algorithm;
import std.stdio;

import scone;

import timedilator.nanoprof;
import timedilator.scopemanager;

// TODO: this struct was probably a bad idea, consider reverting to floats.
/// Rational number-like percentage type, with one-percent precision.
struct Percent
{
    /// Percentage value. Can be positive or negative.
    long value;

    /// Binary operation with an integer (multiply integer by percent, or power percent by integer).
    auto opBinary(string op)( in long other ) const
    {
        static if( op == "*" )  { return (value * other) / 100; }
        static if( op == "^^" ) { return Percent( (value ^^ other) / (100 ^^ (other-1)) ); }
        else static assert( "operator " ~ op ~ " not implemented for Percent" );
    }
}

/** Manages pane layout and focus, and passes input events to focused pane.
 *
 * Takes role of a 'window manager'.
 */
class PaneManager
{
private:
    import std.array;
    // TODO more layouts, like halves, quadrants, 1 sidebar + many vertically arranged windows,
    //      and finally, vim/tmux/i3 style splits
    // TODO use the AwesomeWM concept of tags later to switch between different 'workspaces'
    //     each tag will have an associated layout, and each pane will have associated tags.
    /// Pane manager layouts (think the list of layouts supported by a tiling WM like AwesomeWM).
    enum Layout
    {
        /// Focused pane takes the entire screen and all other panes are hidden.
        Fullscreen
    }

    /// Current pane layout.
    Layout layout = Layout.Fullscreen;

    /// Panes we are managing, including the focused pane.
    Pane[] panes_;

    /// Index of the currently focused pane in panes_. size_t.max when no pane is focused.
    size_t focusedPaneIndex = size_t.max;

public:
    /// Construct a pane manager with no panes and default layout.
    this()
    {
    }

    /// Access all managed panes.
    Pane[] panes()
    {
        return panes_[];
    }

    /// Get the currently focused pane, or null if no pane is focused.
    Pane focusedPane()
    {
        return focusedPaneIndex != size_t.max ? panes_[focusedPaneIndex] : null;
    }

    /// Start managing a pane, and set focus to it.
    void addPane( Pane pane )
    {
        panes_ ~= pane;
        // TODO better focusing logic, changing focus
        focusedPaneIndex = panes_.length - 1;
    }

    /** Update pane manager state for a frame.
     *
     * Recalculates pane bounds based on current layout.
     *
     * Params:
     * width  = Window width in columns.
     * height = Window height in rows.
     */
    void frame( const size_t width, const size_t height )
    {
        final switch( layout )
        {
            case Layout.Fullscreen:
            {
                foreach( idx, pane; panes_ )
                {
                    pane.x = 0;
                    pane.y = 0;
                    pane.width  = width;
                    // Leave 1 row for the statusline
                    pane.height = max(height, 1) - 1;

                    pane.disabled = idx != focusedPaneIndex;
                }
            }
        }
    }

    /** Process an input event and pass it to the proper (focused) pane.
     *
     * Params:
     * input = Event to process.
     */
    void processInputEvent( InputEvent input )
    {
        if( focusedPaneIndex == size_t.max )
        {
            return;
        }
        const shift = input.hasControlKey( SCK.shift );
        switch( input.key )
        {
            // TODO key to jump between exact zoom levels so that one column is exactly one
            //      second/millisecond/microsecond/nanosecond, and use Ctrl-l/Ctrl-h for
            //      panning-by-1-column. This will effectively be the 'reset zoom' key
            // TODO Percent was a bad idea: integer division loses precision.
            //      Use doubles instead. Will lose less precision.
            // TODO acceleration based on how long a key is pressed
            // TODO smooth, continuous panning/zoom when a key is held (probably needs Scone updates)
            // TODO command '1000sg' (vim-style) to go to 1000th second
            //      with variations like '1000mg' for 1000ms, '1000ug' for 1000us and such.
            //      also, '1000sl' will go 1000s right, and 1000uh will go 1000 microseconds right.
            // TODO number prefixes for zoom/pan (100l will go left 100 as much)
            // TODO vim/ranger-like mappings
            // Time zoom.
            case SK.j, SK.down:
                const zoom = shift ? Percent(125) ^^ 3 : Percent(125);
                focusedPane.processCommandZoom( zoom );
                break;
            case SK.k, SK.up:
                const zoom = shift ? Percent(80) ^^ 3 : Percent(80);
                focusedPane.processCommandZoom( zoom );
                break;
            // View panning.
            case SK.h, SK.left:
                const pan = shift ? Percent(-25 * 4) : Percent(-25);
                focusedPane.processCommandPanRelative( pan );
                break;
            case SK.l, SK.right:
                const pan = shift ? Percent(25 * 4) : Percent(25);
                focusedPane.processCommandPanRelative( pan );
                break;
            // Cycle label modes.
            case SK.n:
                focusedPane.processCommandNextLabelMode();
                break;
            // Pause.
            case SK.space:
                focusedPane.processCommandPause();
                break;
            case SK.tab:
                focusedPaneIndex = panes_.empty ? size_t.max : (focusedPaneIndex + 1) % panes_.length;
                break;
            default:
                break;
        }
    }
}

// TODO consider turning panes into pannable viewports of larger-than-pane displays (with a mode for panning?)
/** A single displayed pane, similar to a window on a desktop.
 */
abstract class Pane
{
    import timedilator.tui;

protected:
    /// First column of the pane.
    size_t x;
    /// First row of the pane.
    size_t y;
    /// Width of the pane in columns.
    size_t width;
    /// Height of the pane in row.
    size_t height;

    // TODO: Currently not drawn. Draw after updating to newer Scone after a PR to fix its allocation overhead
    /// Pane background color.
    Color bgColor_   = Color.blackDark;

    /// If true, the pane is disabled and will not be drawn not process events.
    bool disabled_;

public:
    /// Draw the pane using TUI. Should be called every frame.
    void draw( TUI tui );

    // All panes have a consistent set of commands, but those commands may be
    // interpreted differently depending on pane. This should make pane controls
    // consistent and intuitive.

    /** Process a zoom command, multiplying zoom level by specified value.
     *
     * Returns: true if the event was processed, false otherwise.
     */
    bool processCommandZoom( const Percent percent ) { return false; }

    /** Process a panning command, moving the view by current view width multiplied by specified value.
     *
     * Returns: true if the event was processed, false otherwise.
     */
    bool processCommandPanRelative( const Percent percent ) { return false; }

    /** Process a pause command, pausing automatic view updates.
     *
     * Returns: true if the event was processed, false otherwise.
     */
    bool processCommandPause() { return false; }

    /** Process a 'next label' command, cycling label (e.g. scope label) modes.
     *
     * Returns: true if the event was processed, false otherwise.
     */
    bool processCommandNextLabelMode() { return false; }

    /** Set the disabled status of the pane.
     */
    void disabled( const bool disabled ) { disabled_ = disabled; }
}

import timedilator._scope;
/** Color used to display merged scopes (and no other scopes).
 *
 * Merged scopes are view-only scopes created by merging multiple scopes too small to be drawn.
 */
enum MergedColor = Color.white;

/// Color used for major ('thick') grid lines.
enum GridColorMajor = Color.white;
/// Color used for minor ('subgrid') grid lines.
enum GridColorMinor = Color.whiteDark;

/** Get a background color to use for given scope.
 *
 * Tries to 'randomize' colors to achieve good contrast between scopes.
 * May become smarter in future when we have RGB color support (e.g. using
 * color to show scope latency variance, name prefix, and so on).
 *
 * TODO: we can do this even sooner, using e.g. blue/green/yellow/red to show
 * orders of magnitude on latency variance, or scope length, or any one of a
 * number of variables. Some of these (e.g. latency variance) will require more
 * work elsewhere.
 */
Color scopeBgColor( in Scope scopeToColor )
{
    // Special color reserved for merged scopes.
    if( scopeToColor.id == TimepointIDMerged )
    {
        return MergedColor;
    }
    static const ScopeColors = 
        [Color.red,
         Color.green,
         Color.yellow,
         Color.blue,
         Color.magenta,
         Color.cyan,
         Color.redDark,
         Color.greenDark,
         Color.yellowDark,
         Color.blueDark,
         Color.magentaDark,
         Color.cyanDark,
         Color.whiteDark
        ];
    return ScopeColors[ scopeToColor.id % ScopeColors.length ];
}

/// Modes used to show different information using scope labels.
enum ScopeLabelMode: ubyte
{
    /// Default mode - tries to display multiple data items that are most important.
    Default,
    /// Show scope name.
    Name,
    /// Show scope duration.
    Duration,
    /// Show scope start time.
    StartTime,
    /// Show scope nest level.
    NestLevel,
    /// Show scope timepoint ID.
    TimepointID
    // TODO modes to show latency variance and other useful, possibly calculated, data
}

/** A pane showing scopes in a single nanoprof stream in real-time.
 *
 * Support pausing, zoom and panning.
 */
class SimpleStreamPane: Pane
{
    import std.array;
    import std.conv;
private:
    /** Data computed for each viewed scope at the beginning of a frame.
     */
    struct ScopeData
    {
        /// Scope label (text drawn for the scope).
        char[62] label = ' ';
        /// Number of cells of the label drawn to the screen so far.
        ushort   labelCellsDrawn = 0;

        /// Get the label character to draw in the current cell.
        dchar currentChar() const
        {
            // Draw a border on the first character so we can distinguish scopes of same color.
            if( labelCellsDrawn == 0 )
            {
                // TODO: once we move to Scone v3, update to use one of the Unicode
                //       left border characters, especially the first one here:
                //       '▏' '▎' '▍'
                return '[';
            }
            const charIdx = labelCellsDrawn - 1;
            return charIdx >= label.length ? ' ' : label[charIdx];
        }
    }

    /// Determines what we draw on the scope labels right now.
    ScopeLabelMode labelMode = ScopeLabelMode.Default;

    /// We read profiled scope information from here.
    ScopeManager scopeManager;

    /// ID of the stream we're viewing.
    StreamID stream;

    /// Size of the time window currently visible in the pane (time delta from left to right end of pane).
    Time timeWindowSize = Time.msecs(2000);

    /// End of the time window for the current frame.
    Time timeWindowEnd = Time(0);

    /// Start of the time window for the current frame.
    Time timeWindowStart() const { return timeWindowEnd - timeWindowSize; }

    /** Do not allow time window size to go below this (zoom in limit).
     *
     * Enough to show a nanosecond as a single char on a 100-char wide terminal.
     */
    enum Time timeWindowSizeMin = Time.nsecs(100);

    /// Do not allow time window size to go above this (zoom out limit).
    enum Time timeWindowSizeMax = Time.secs(100);

    /// Memory to store the scopes viewed in the current frame in.
    Scope[] trackViewBuffer;

    /** Computed information about the scopes we're viewing, e.g. labels.
     *
     * Matches indices in current frame's scope view.
     */
    ScopeData[] scopeDataBuffer;

    /** If true, we pan automatically to the latest scopes as they are being added.
     *
     * TODO allow panning in auto mode to view at a 'time delta' relative to latest scope.
     *
     * If false, we allow the user to pan manually.
     */
    bool autoPan = true;

    /** The smallest scope we can theoretically display fits into one 8-th of a column.
     *
     * Note that we can never display more than 2 scopes in a column since we
     * can only have 2 colors per cell.
     *
     * Note that this is not yet utilized, we need Scone to work with Unicode
     * characters. We do use ASCII tricks to display 2 scopes in a column, though.
     */
    enum maxResolutionPerColumn = 8;

    /// Total number of scopes currently displayed (stored in trackView).
    size_t scopeCount = 0;

public:
    import timedilator.tui;
    /** Construct a SimpleStreamPane viewing scopes of specified stream from a scope manager.
     *
     * Params:
     * rhsScopeManager = Scope manager managing profiling data.
     * rhsStream       = ID of the stream ti view.
     */
    this( ScopeManager rhsScopeManager, in StreamID rhsStream )
    {
        scopeManager    = rhsScopeManager;
        stream          = rhsStream;
        // Good initial buffer sizes - are expanded as needed at runtime.
        trackViewBuffer = new Scope[1024 * 8];
        scopeDataBuffer = new ScopeData[1024 * 8];
    }

    override bool processCommandZoom( const Percent percent )
    {
        if( disabled_ ) { return false; }
        timeWindowSize = clamp( Time(percent * timeWindowSize.picoseconds), timeWindowSizeMin, timeWindowSizeMax );
        return true;
    }

    override bool processCommandPanRelative( const Percent percent )
    {
        if( disabled_ ) { return false; }
        timeWindowEnd = timeWindowEnd + Time( percent * timeWindowSize.picoseconds );
        return true;
    }

    override bool processCommandPause()
    {
        if( disabled_ ) { return false; }
        autoPan = !autoPan;
        return true;
    }

    override bool processCommandNextLabelMode()
    {
        if( disabled_ ) { return false; }
        stderr.writeln( "labelmode",  labelMode, labelMode + 1, labelMode == ScopeLabelMode.max ? ScopeLabelMode.min : labelMode + 1 );
        labelMode = cast(ScopeLabelMode)(labelMode == ScopeLabelMode.max ? ScopeLabelMode.min : labelMode + 1);
        return true;
    }

    override void draw( TUI tui )
    {
        if( disabled_ ) { return; }

        // Time we got newest data for this stream - used for autopanning.
        const mostRecentUpdateTime = scopeManager.lastUpdateTime( stream );
        // Stream was never updated, so it has no scopes, so there's nothing to draw.
        if( mostRecentUpdateTime == Time( 0 ) )
        {
            // TODO display something here to tell user we're drawing a stream with no scopes.
            return;
        }

        // Autopanning keeps the view on the most recently updated data.
        if( autoPan )
        {
            timeWindowEnd = mostRecentUpdateTime;
        }

        // TODO uncomment when we have new Scone (may need to write a PR to reduce allocations)
        // tui.drawPaneBackground( bgColor_, x, y, width, height );
        drawTimeGrid( tui );
        drawTracks( tui );
        setPaneStatus( tui );
    }


private:
    /** Time window represented by a single text column.
     *
     * E.g. if our pane is 20 columns wide and viewing a time window of 20 seconds,
     * a scope exactly 1 column wide has a duration of 1 second.
     */
    Time colWindow() const { return timeWindowSize / width; } 

    /** Update the pane part of the statusline for current frame.
     *
     * TODO: figure out how statuslines will work w/ multiple panes.
     *       One statusline per pane, or a global statusline set by focused pane?
     *       Or global statusline for entire program + pane statuslines for panes?
     *
     * Shows current label mode, zoom level, pan offset, resolution etc.
     *
     * Params:
     * tui = TUI to set the statusline with.
     */
    void setPaneStatus( TUI tui)
    {
        // TODO Color constants for all colors currently hardcoded here.
        // Whether we're autopanning or not.
        if( autoPan ) { tui.addPaneStatus( Color.yellow, "auto  " ); }
        else          { tui.addPaneStatus( Color.blue,   "manual" ); }
        // Current label mode.
        final switch( labelMode ) with( ScopeLabelMode )
        {
            case Default:     tui.addPaneStatus( Color.greenDark,   "default " ); break;
            case Name:        tui.addPaneStatus( Color.green,       "name    " ); break;
            case Duration:    tui.addPaneStatus( Color.blueDark,    "duration" ); break;
            case StartTime:   tui.addPaneStatus( Color.blue,        "start   " ); break;
            case NestLevel:   tui.addPaneStatus( Color.magentaDark, "nest    " ); break;
            case TimepointID: tui.addPaneStatus( Color.magenta,     "id      " ); break;
        }

        // TODO to!string calls here add up to 1% of overhead; optimize that.
        // Need a no-alloc no-string-format version of Time.toString and use
        // toChars for scopeCount.

        // Visible scope count, mostly for performance monitoring.
        tui.addPaneStatus( Color.blueDark, scopeCount.to!string );
        // Time position / pan.
        const startTime = scopeManager.startTime();
        char[64] charBuffer;
        if( timeWindowEnd > startTime )
        {
            const timePosition = timeWindowEnd - startTime;
            auto timeString = timePosition.writeFixedPrecision( charBuffer[] );
            tui.addPaneStatus( Color.yellowDark, timeString.idup );
        }
        else
        {
            tui.addPaneStatus( Color.yellowDark, "before start" );
        }
        // TODO use colors to highlight column resolution in nsecs (red) /
        //      usecs (dark yellow) / msecs(light yellow) / secs (green)
        // Column resolution.
        auto colWindowString = colWindow.writeFixedPrecision( charBuffer[] );
        tui.addPaneStatus( Color.cyanDark, colWindowString.idup );
    }

    /// Calculate the time difference between 2 vertical lines of the time guiding grid.
    Time calculateGridScale() const
    {
        // TODO something more exact/consistent.
        // E.g. grid scale exists in levels, first at 1ns, second at 10ns, third
        // at 100ns etc, and always display the highest grid scale that we can
        // fit at least 20 lines of into the pane, and draw every 10th pip as
        // a 'big line'. Then we can also easily draw grid scale on statusline.
        // TODO display grid scale on statusLine (after we get consistent scale)
        const columnsPerGridLineDesired = 8;
        const gridLinesDesired   = width / columnsPerGridLineDesired;
        Time gridScale = Time.nsecs(100);
        for(;;)
        {
            const timeWindowCoveredByGrid = gridScale * gridLinesDesired;
            if( timeWindowCoveredByGrid >= timeWindowSize )
            {
                break;
            }
            gridScale = gridScale * 2;
        }
        return gridScale;
    }

    /** Draw the time guiding grid at the top/bottom of the pane.
     *
     * Params:
     * tui = TUI to draw into.
     */
    void drawTimeGrid( TUI tui )
    {
        const gridScale = calculateGridScale();
        const maxY = y + height - 1;
        const maxX = x + width - 1;

        const colWindow = this.colWindow();
        assert( gridScale > colWindow, "Unexpected result of the gridScale calculation" );
        enum gridYSize = 2;

        // Drawing the grid row-by-row, which is a bit unintuitive since the
        // grid consists of vertical lines.
        for( size_t row = y; row <= maxY; ++row )
        {
            const borderDistance = min(row - y, maxY - row);
            // Draw only on the top/bottom of the screen (reduces TUI overhead).
            if( borderDistance >= gridYSize )
            {
                continue;
            }
            // Set the first grid line's time to a multiple of grid scale.
            Time nextGridTime = timeWindowStart.alignUpToMultipleOf( gridScale );
            size_t gridLineIdx = 0;
            for( size_t col = x; col <= maxX; ++col )
            {
                const Time colStart = timeWindowStart + colWindow * col;
                const Time colEnd   = colStart + colWindow;
                const intersection  = nextGridTime >= colStart && nextGridTime < colEnd;
                // Only draw the grid line if 'nextGridTime' intersects with this column.
                if( !intersection )
                {
                    continue;
                }

                const bigLine = gridLineIdx % 10 == 0;
                const color = bigLine ? GridColorMajor : GridColorMinor;
                if( bigLine || borderDistance + 1 < gridYSize )
                {
                    tui.drawCell( Color.same, color, '|',  col, row );
                }
                nextGridTime = nextGridTime + gridScale;
                ++gridLineIdx;
            }
        }
    }

    /** Draw all scopes as 'tracks', one per nest level.
     *
     * This is the most important information shown by this pane.
     * Params:
     * TUI = TUI to draw to.
     */
    void drawTracks( TUI tui )
    {
        // Reset scope count.
        scopeCount = 0;
        // TODO compress the view vertically, hiding non-available levels so we
        // can see e.g. level -128. Display nest level number on the left of
        // each line. Also support panning up/down if we can't fit in pane. Show
        // arrows on top/bottom of screen if there is more stuff in that direction.

        // Draw tracks of individual nest levels.
        foreach( NestLevel nestLevel; NestLevel.min .. NestLevel.max + 1 )
        {
            const maxY = y + height - 1;
            const int trackRow = cast(int)(y + height / 2) - nestLevel;
            // Only draw the track if it fits pane boundaries.
            if( trackRow >= y && trackRow <= maxY )
            {
                drawTrack( tui, nestLevel );
            }
        }
    }

    /** Information about a scope intersecting current cell while drawing a track.
     */
    struct CellIntersection
    {
        /// Index of the scope in current scope view (see drawTrack code).
        size_t scopeIndex;
        /** How many percent of the cell is 'taken up' by the scope.
         *
         * If 100, the entire cell is part of the scope and can be drawn with
         * its color. If less than 100 but a large portion, we may still
         * 'partially draw' with the scope's color, using tricks like (TODO)
         * Unicode block elements or just plain ASCII with different
         * foreground/background colors.
         */
        size_t coveragePercent;
    }

    /** Draw a single 'track', drawing all scopes on the same nest level.
     *
     * Params:
     * tui       = TUI to draw with.
     * nestLevel = Nest level to draw.
     */
    void drawTrack( TUI tui, in NestLevel nestLevel )
    {
        // The smallest scope time we can theoretically display (in some cases) is 8th of a character wide.
        // (Note: currently this is not the case - we need Unicode support in Scone first)
        const Time timeResolution = max(Time(1), timeWindowSize / (width * maxResolutionPerColumn));

        // Get a copy of scopes that are large enough to be visible and fit in the time window.
        auto view = scopeManager.trackView( 
            stream,
            nestLevel,
            timeWindowStart,
            timeWindowEnd,
            timeResolution,
            trackViewBuffer[] );

        if( view.empty )
        {
            return;
        }

        scopeCount += view.length;
        // Calculate auxiliary data for each viewed scope.
        initScopeData( view );

        // Row (in window coordinates) to draw the track on.
        const int trackRow = cast(int)(y + height / 2) - nestLevel;
        // Index of the current scope being drawn.
        size_t scopeIdx = 0;
        // Skip any scopes that end before the time window (and therefore the first column) starts.
        while( scopeIdx < view.length && view[scopeIdx].end < timeWindowStart )
        {
            scopeIdx++;
        }
        // Draw scopes cell by cell.
        // scopeIdx gets updated whenever current scope changes in a cell.
        for( size_t col = 0; col < width && scopeIdx < view.length; col++ )
        {
            CellIntersection[maxResolutionPerColumn + 1] intersectBuffer;
            auto intersections = 
                scopesIntersectingCell( view, scopeIdx, col, intersectBuffer[] );

            if( !intersections.empty )
            {
                drawTrackCell( tui, x + col, trackRow, view, intersections );
            }
        }
        // If we ran out of view buffer space this frame, enlarge the buffer for the next.
        if( view.length > trackViewBuffer.length / 2 )
        {
            trackViewBuffer.length = trackViewBuffer.length * 2;
            scopeDataBuffer.length = scopeDataBuffer.length * 2;
        }
    }

    /** Init label for given scope, writing it to corresponding scopeData.
     *
     * What the label will be depends on label mode.
     *
     * Params:
     * _scope    = Scope to init label for.
     * scopeData = Store calculated per-frame scope data (such as the label) here.
     * startTime = Start time of the profiling, for relative time values.
     */
    void initScopeLabel( in Scope _scope, ref ScopeData scopeData, const Time startTime )
    {
        // TODO labels: shortcuts + legend for smallest scopes. Predict scope
        // draw size from its size and generate a shortcut name for it.
        // 1-char scopes: 1-9/A-Z. 2-char scopes: 2-digit combinations.
        auto label = scopeData.label[];
        final switch( labelMode ) with( ScopeLabelMode )
        {
            // Max useful information by default: name + duration.
            case Default:
                const nameChars = min( label.length, _scope.name.length );
                label[0 .. nameChars] = _scope.name[0 .. nameChars];
                label[nameChars .. min(nameChars + 1, label.length)] = ':';
                auto duration = _scope.duration.writeFixedWidth!5( label[nameChars + 1 .. $] );
                label[nameChars + 1 + duration.length .. $] = ' ';
                break;
            case Name:
                const nameChars = min( label.length, _scope.name.length );
                label[0 .. nameChars] = _scope.name[0 .. nameChars];
                label[nameChars .. $] = ' ';
                break;
            case Duration:
                auto duration = _scope.duration.writeFixedWidth!5( label[0 .. $] );
                label[duration.length .. $] = ' ';
                break;
            case StartTime:
                const timeDelta = _scope.start - startTime;
                auto timeString = timeDelta.writeFixedPrecision( label[0 .. $] );
                label[timeString.length .. $] = ' ';
                break;
            case NestLevel:
                auto chars = (cast(int)(_scope.nestLevel)).toChars();
                label[chars.length .. $] = ' ';
                chars.copy( label[]);
                break;
            case TimepointID:
                auto chars = _scope.id.toChars();
                label[chars.length .. $] = ' ';
                chars.copy( label[]);
                break;
        }
        scopeData.labelCellsDrawn = 0;
    }

    /** Compute auxiliary data for scopes viewed during a frame.
     *
     * Params:
     * view = Scopes on a single nest level drawn during current frame.
     */
    void initScopeData( in Scope[] view )
    {
        // Process each scope, and init the scopeDataBuffer value for it.
        const startTime = scopeManager.startTime();
        foreach( i, ref _scope; view )
        {
            assert( scopeDataBuffer.length > i );
            initScopeLabel( _scope, scopeDataBuffer[i], startTime );
        }
    }

    /** Get information about which scopes from a view intersect with a cell
     * at specified column and how much of the cell they take up.
     *
     * Params:
     * view            = Scopes in current track that may be visible on the pane.
     * scopeIdx        = Index of current scope, which may intersect the cell.
     *                   This scope must end after the beginning of this column.
     *                   When scopesIntersectingCell() returns, scopeIdx will
     *                   point to a scope that ends after the beginning of
     *                   the next column (but may start before that).
     * col             = Column of the cell we're testing for intersection with.
     * intersectBuffer = Buffer to write intersections into. Must be large
     *                   enough to fit maxResolutionPerColumn intersections.
     *
     * Returns: Slice of intersectBuffer containing all found intersections.
     */
    CellIntersection[] scopesIntersectingCell(
        in Scope[] view,
        ref size_t scopeIdx,
        in size_t col,
        CellIntersection[] intersectBuffer) const
    {
        assert( scopeIdx < view.length );
        const Time colStart  = timeWindowStart + colWindow * col;
        assert( view[scopeIdx].end > colStart,
                "When processing a column, first scope must end after the column starts");
        const colWindow = this.colWindow();
        const Time colEnd = colStart + colWindow;
        // If there is a gap between scopes, the scope may not start within
        // current column, in which case we can skip the column.
        if( view[scopeIdx].start >= colEnd ) { return intersectBuffer[0 .. 0]; }

        size_t intersectCount = 0;
        // Process scopes that intersect with current column. We bail out when
        // we hit a scope that ends after the column window - we don't increase
        // scopeIdx for that scope as it also intersects with the next column.
        for( ; scopeIdx < view.length; scopeIdx++ )
        {
            // scope end is >= colStart and scope start is < colEnd; we have an
            // intersection. Smallest scopes that should be returned are
            // colWindow/maxResolutionPerColumn wide.
            // At most 1 more may intersect a cell: when the cell has 1 partial
            // intersection from start and end each and maxResolutionPerColumn-1
            // full intersections in the middle.
            import std.format;
            assert( intersectCount < maxResolutionPerColumn + 1 ,
                "ScopeManager.trackView() with specified time resolution " ~
                "should not return more than %s scopes intersecting with a column"
                .format(maxResolutionPerColumn) );

            // Clamp scope bounds to the column bounds to get the intersection.
            const Time scopeIntersectStart  = max( colStart, view[scopeIdx].start );
            const Time scopeIntersectEnd    = min( colEnd,   view[scopeIdx].end );
            // This may not be true if the scope starts after the view - in which case
            // scopeIntersectStart == view[scopeIdx].start, scopeIntersectEnd == colEnd
            if( scopeIntersectStart <= scopeIntersectEnd )
            {
                const Time scopeIntersectWindow = scopeIntersectEnd - scopeIntersectStart;
                const coveragePercent = (scopeIntersectWindow.picoseconds * 100) / colWindow.picoseconds;
                intersectBuffer[intersectCount++] = CellIntersection( scopeIdx, coveragePercent );
            }

            if( view[scopeIdx].end > colEnd )
            {
                // The scope continues into the next column - do not increase
                // scopeIdx as it will be needed for the next column.
                break;
            }
        }
        return intersectBuffer[0 .. intersectCount];
    }

    /** Draw a single cell of a scope track, trying to pack as much scope 
     * information as possible.
     *
     * If multiple scopes intersect with this cell, we try to represent that by
     * drawing a character on a background where the character has a different
     * color from the background.
     *
     * If a single scope intersects with the cell, it likely takes up multiple
     * cells and we draw a character of that scope's label, so the cells of the
     * scope will make up a complete-ish readable label.
     *
     * Params:
     * tui           = TUI to draw to.
     * col           = Column of the cell to draw.
     * row           = Row of the cell to draw.
     * view          = Slice of all potentially visible scopes for this track.
     * intersections = Information about scopes intersecting with the cell
     *                 (i.e. the scopes that should determine cell color).
     */
    void drawTrackCell(
        TUI tui,
        in size_t col,
        in size_t row,
        in Scope[] view,
        in CellIntersection[] intersections )
    {
        // Manually getting the top 2 intersections as topNIndex was
        // slow enough to show up in profiler output, so I wrote it manually.
        // Get indices to 2 largest intersections.
        size_t[2] maxIntersectionIndices;
        size_t maxCoveragePercent = 0;
        foreach( i, intersection; intersections )
        {
            if( intersection.coveragePercent >= maxCoveragePercent )
            {
                maxCoveragePercent = intersection.coveragePercent;
                maxIntersectionIndices[1] = maxIntersectionIndices[0];
                maxIntersectionIndices[0] = i;
            }
        }

        const oneIntersection = intersections.length == 1;
        const maxIntersection = intersections[maxIntersectionIndices[0]];

        // The thresholds here (70, 30, 50) are arbitrary and may be tweaked for
        // better results.

        // If there is a single dominant intersection, draw its color.
        if( maxIntersection.coveragePercent >= 70 )
        {
            const charToDraw = scopeDataBuffer[maxIntersection.scopeIndex].currentChar;
            scopeDataBuffer[maxIntersection.scopeIndex].labelCellsDrawn++;
            const color = scopeBgColor( view[maxIntersection.scopeIndex] );
            tui.drawCell( color, color.contrastingTextColor(), charToDraw,  col, row );
            return;
        }
        // TODO consider also drawing in full color if oneIntersection && maxIntersection.coveragePercent >=50
        // If there is only one intersection but it's not very dominant, draw that as a partial color.
        if( oneIntersection && maxIntersection.coveragePercent >= 30 )
        {
            const bgColor = Color.same;
            const fgColor = scopeBgColor( view[maxIntersection.scopeIndex] );
            tui.drawCell( bgColor, fgColor, '#',  col, row );
            return;
        }
        // If there are 2 major intersections, draw the larger one as background, smaller one as foreground.
        if( !oneIntersection )
        {
            const secondMaxIntersection = intersections[maxIntersectionIndices[1]];
            if( maxIntersection.coveragePercent >= 30 && secondMaxIntersection.coveragePercent >= 30 )
            {
                const bgColor = scopeBgColor( view[maxIntersection.scopeIndex] );
                const fgColor = scopeBgColor( view[secondMaxIntersection.scopeIndex] );
                tui.drawCell( bgColor, fgColor, '#',  col, row );
                return;
            }
        }
        const totalCoveragePercent = intersections.map!( i => i.coveragePercent ).sum;
        // TODO consider also drawing a '#' in merged color when totalCoveragePercent > 30.
        // If there is a bunch of small intersection, draw using the 'merged' color.
        if( totalCoveragePercent >= 50 )
        {
            tui.drawCell( MergedColor, Color.black, ' ',  col, row );
        }
    }
}

// TODO despiker-like () pan 
// TODO may share some code with SimpleStreamView and maybe even be derived from it
// (a frame view is basically a bounded stream view, with extra key bindings
//  for next/previous frame where frames are neighboring same-level+same-id scopes)
// TODO Shift-hjkl: previous frame, parent frame, child frame, next frame
// TODO way to jump to 'worst frame' matching current scope (same id)
// TODO histogram of frames matching current scope
/** A view for frame-based profiling, always showing a subtree of one scope.
 *
 * The viewed scope acts as a 'frame', with ways of going up/down the hierarchy
 * to define current 'frame'. Can also jump to previous/next scope of same kind
 * as if jumping between frames.
 *
 * Similar to Despiker (older project by F Majerech) GUI.
 */
class SimpleFramePane: Pane
{
    import timedilator.tui;
    override void draw( TUI tui )
    {
        assert( false, "TODO" );
    }
}
