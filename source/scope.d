// Copyright (C) 2020 Ferdinand Majerech
//
// This file is part of Timedilator.
//
// Timedilator is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Timedilator is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Timedilator.  If not, see <http://www.gnu.org/licenses/>.

module timedilator._scope;

import timedilator.nanoprof;
import timedilator.datasource;


/** Represents times as well as durations.
 *
 * Time is measured in nanosecond since epoch defined by profiled process.
 * Time values are calculated from HighPrec events in profile data.
 *
 * Not using std.datetime structures since they work in hectonanoseconds - we
 * work in picoseconds so we can keep up with high-frequency CPUs and so we
 * can minimize precision losses while using integer arithmetic.
 */
struct Time
{
    import std.conv;
    import std.algorithm;
    // TODO more bits. 96 ought to be enough for everybody. Or maybe a library cent type.
    // 64bit picosecond value wraps after 213.5 days.

    /// Time in picoseconds.
    ulong picoseconds = ulong.max;

    /// Special Time value representing infinite future or infinite duration.
    enum Infinity = Time( ulong.max );

    /// Construct a Time from its picosecond value.
    this( in ulong initPicoseconds )
    {
        picoseconds = initPicoseconds;
    }

    /// Construct a Time from nanoseconds.
    static Time nsecs( const ulong value )
    {
        return Time( value * 1000 );
    }

    /// Construct a Time from microseconds.
    static Time usecs( const ulong value )
    {
        return Time( value * 1000000 );
    }

    /// Construct a Time from milliseconds.
    static Time msecs( const ulong value )
    {
        return Time( value * 1000000000 );
    }

    /// Construct a Time from seconds.
    static Time secs( const ulong value )
    {
        return Time( value * 1000000000000UL );
    }

    /// Is this time an infinity / is this duration infinite?
    bool infinite() const { return picoseconds == ulong.max; }

    /// Comparison operator for e.g. sorting.
    int opCmp( in Time comparee ) const
    {
        // Using branches to avoid overflow issues.
        return picoseconds < comparee.picoseconds  ? -1 :
               picoseconds == comparee.picoseconds ?  0 :
                                                      1;
    }

    /** Binary operators between Times (often acting as durations).
     *
     * * Subtracting two times gives us the duration between them.
     * * Adding/subtracting a duration to a time moves that time by that duration.
     * * Dividing one duration by another tells us how many times the second fits into the first.
     */
    Time opBinary(string op)( in Time other ) const
    {
        static if( op == "+" ) { return Time( picoseconds + other.picoseconds); }
        static if( op == "-" ) { return Time( picoseconds - other.picoseconds); }
        static if( op == "/" ) { return this / other.picoseconds; }
        else static assert( "operator " ~ op ~ " not implemented" );
    }

    /** Binary operators between Time (used as duration) and integer.
     *
     * Multiplying/dividing a duration by an integer multiplies/divides the length of that duration.
     */
    Time opBinary(string op)( in ulong other ) const
    {
        static if( op == "*" ) { return Time( picoseconds * other ); }
        static if( op == "/" ) { return Time( picoseconds / other ); }
        else static assert( "operator " ~ op ~ " not implemented" );
    }

    /** Align a time/duration up to a multiple of duration, and return aligned value.
     *
     * Used for calculating the next time a periodic event should take place.
     */
    Time alignUpToMultipleOf( in Time base ) const
    {
        return Time( ((picoseconds + base.picoseconds - 1) / base.picoseconds) * base.picoseconds );
    }

    /** Convert a Time to human-readable string.
     *
     * Not particularly efficient, but can be used multiple times per frame.
     */
    string toString() const
    {
        import std.format;
        ulong thousandths;
        const u = unit( thousandths );
        return "%.3f%s".format( cast(real)(thousandths) * 0.001, u );
    }

    /** Get the name of the unit to use for most readable display of this Time value,
     * as well as the Time value converted to thousandths of that unit.
     *
     * Allows us to output time in a decimal format with up to 3 digits above decimal
     * point and 3 digits below decimal point, e.g. `"123.456us"`.
     */
    string unit( out ulong thousandths) const
    {
        static immutable units = ["ns", "us", "ms", "s", "ks", "Ms", "Gs"];
        if( infinite ) { return "infinity"; }
        thousandths = picoseconds;
        size_t unitIndex = 0;
        for( ; thousandths >= 1000000; ++unitIndex)
        {
            thousandths /= 1000;
        }
        return units[unitIndex];
    }

    // TODO: Written for minimum overhead. If needed, rewrite as a more general
    //       algorithm to support any precision, and perhaps merge writeFixedWidth
    //       with writeFixedPrecision.
    /** Write the Time value to a fixed-width output with up to 6 digits of precision.
     *
     * Will choose the proper time units so that at most 3 digits are above the
     * decimal point.
     *
     * TParams:
     * chars = Max number of chars to write to target. Must be at least 5
     *         (2 chars for unit, 1/2 or 0/3 for decimal point/digits of precision).
     *         More than 9 does not make sense (2 unit, 1 decimal point, 6 precision).
     * Params:
     * target = Buffer to write to. If shorter than 'chars', result will be truncated.
     *          (we could require `chars` chars but then it'd be inconvenient to call
     *          in some cases, and we need fixed `chars` so the result is not too wide
     *          on screen).
     * Returns: Slice of `target` containing written data.
     */
    char[] writeFixedWidth(size_t chars)( char[] target ) const
    {
        static assert( chars >= 5, "writeFixedWidth needs at least 5 characters of space" );
        enum unitChars  = 2;
        if( infinite )
        {
            "open".copy( target[] );
            return target[0 .. "open".length];
        }
        enum valueChars = chars - unitChars;

        // Current implementation repeatedly divides the picosecond value by
        // 1000 until we have a value below 1000_000 that we can use to
        // represent 'thousandths' of a unit (e.g. nanoseconds). That's why
        // we have at most 6 digits of precision.
        // We can have less than 6 - e.g. 4 digits when we only have 'tens of
        // thousands of thousandths of a unit'

        // Duration in thousandths of unit.
        ulong dur;
        const unit = unit( dur );

        // Value above the decimal point.
        auto beforeRadix = (dur / 1000).toChars();
        char[chars] buffer = ' ';
        assert( buffer.length > beforeRadix.length, "buffer must be at least 3 chars wide" );
        beforeRadix.copy( buffer[] );
        size_t usedChars = beforeRadix.length;
        // If we can fit the decimal point.
        if( usedChars + 1 < valueChars )
        {
            // Write the decimal point.
            buffer[usedChars++] = '.';
            // Write the chars after the decimal point, if we have space.
            auto afterRadix = (dur % 1000).toChars();
            const afterRadixChars = min(afterRadix.length, valueChars - usedChars);
            afterRadix[0 .. afterRadixChars].copy( buffer[usedChars .. $] );
            usedChars += afterRadixChars;
        }
        assert( usedChars <= valueChars );

        // Write the unit.
        unit.copy( buffer[usedChars .. $] );
        usedChars += unit.length;

        // Truncate the result if needed and write it out.
        auto result = target[0 .. min( target.length, usedChars )];
        buffer[0 .. result.length].copy( result );
        return result;
    }

    // TODO draw this like:
    // 123s124m321u143n (124 secs, 124 millisecs, 321 microsecs and 143 nanosecs)
    /** Write the Time to string as a (high) fixed-precision value to a buffer - no allocations.
     *
     * Currently prints the results in microseconds with 3 decimals after radix
     * (so basically nanoseconds). Less compact and readable than writeFixedWidth,
     * but more precise and comparable.
     *
     * Used when we need absolute 'current time' values, such as time start
     * positions of scopes - fixed width format is useless there as successive
     * scopes with higher start times would lose precision.
     *
     * Params:
     * target = Buffer to write to. Result may be truncated to fit it.
     *
     * Returns: Slice of `target` containing written data.
     */
    char[] writeFixedPrecision( char[] target ) const
    {
        // 40 should be enough for 128-bit time values, 10 more adds space for radix, units, etc.
        char[50] buffer;
        const microseconds = picoseconds / 1000000;
        const nanoseconds  = picoseconds % 1000000 / 1000;
        auto chars = microseconds.toChars();
        auto usedChars = chars.length;
        chars.copy( buffer[] );
        buffer[usedChars++] = '.';
        chars = nanoseconds.toChars();
        chars.copy( buffer[usedChars .. $] );
        usedChars += chars.length;
        "us".copy( buffer[usedChars .. $] );
        usedChars += "us".length;

        auto result = target[0 .. min( target.length, usedChars )];
        buffer[0 .. result.length].copy( result );
        return result;
    }
}

/// ID/index of a nanoprof data stream (e.g. a thread of profiled program).
alias StreamID = ushort;

/** Instance of a profiled scope; represents a task, when that task happened and how long it took.
 *
 * Examples: processing a packet, sorting an array, an event loop, an iteration of that event loop, etc.
 *
 * Starts at a recorded timepoint, ends at the next timepoint at same or lower nest level.
 *
 * May be stored in non-GC memory - any references to GC memory from Scope must 
 * also be referenced from elsewhere so that they don't get collected.
 */
struct Scope
{
    /// Name of the scope. Owned by TimepointRegistry, not by the Scope.
    string name;
    /// Time when the scope was entered.
    Time start;
    /// Duration of the scope. May be infinity if the scope was not exited yet.
    Time duration;
    /** Position to start reading if we want to read any forgotten subscopes of this scope.
     *
     * We can start reading here (and read up until restartPoint of the next scope on same level
     * to re-read dropped subscopes of this scope.
     */
    RestartPoint restartPoint;
    /// ID of the nanoprof stream (e.g. thread of profiled process) the scope comes from.
    StreamID stream;
    /// ID of the timepoint event that started the scope. Determines name and nest level.
    TimepointID id;
    /** Nest level of the scope.
     *
     * Higher nest levels nest in lower nest levels, except -128, which is used for internal profiling.
     */
    NestLevel nestLevel;

    /** Time when the scope was exited.
     *
     * May be infinity if the scope was not exited yet.
     */
    Time end() const 
    {
        return open ? Time.Infinity : start + duration;
    }

    /** Set the time when the scope was exited.
     *
     * Called when we determine the exit time for an open scope, or when
     * re-reading a forgotten scope to fit it before following scopes.
     *
     * Params:
     * time = Time the scope will end at. Must be greater or equal to start.
     */
    void end( in Time time )
    {
        assert( time >= start, "cannot set scope end before its start" );
        duration = time.infinite ? Time.Infinity : time - start;
        assert( end == time, "end set to unexpected value" );
    }

    /// True if the scope has not yet exited (we don't have the timepoint event that ends it).
    bool open() const { return duration.infinite; }

    /// Comparison for scope ordering/sorting by start time.
    int opCmp( in Scope comparee ) const
    {
        return start.opCmp( comparee.start );
    }
}
