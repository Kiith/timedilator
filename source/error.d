// Copyright (C) 2020 Ferdinand Majerech
//
// This file is part of Timedilator.
//
// Timedilator is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Timedilator is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Timedilator.  If not, see <http://www.gnu.org/licenses/>.

module timedilator.error;

/** Error status / type.
 */
enum TUIErrorType: uint
{
    /// No error.
    OK,
    /// I/O error while reading nanoprof data stream (e.g. error reading from file).
    StreamReadError,
    /// Format error while reading nanoprof data stream (e.g. unparsable event type byte).
    StreamFormatError
}

// TODO eventually make the code mostly nothrow, and process all errors into 'TUIError'.

/// Error struct for returning (instead of throwing).
struct TUIError
{
    /// Error type.
    TUIErrorType errorType = TUIErrorType.OK;
    /// Human-readable error message.
    string humanMessage = "OK";

    /// If true, there is no error.
    bool ok() const { return errorType == TUIErrorType.OK; }
}

/// Construct a StreamReadError TUI error with given message.
TUIError streamReadError( in string message )
{
    return TUIError( TUIErrorType.StreamReadError, message );
}

/// Construct a StreamFormatError TUI error with given message.
TUIError streamFormatError( in string message )
{
    return TUIError( TUIErrorType.StreamFormatError, message );
}
