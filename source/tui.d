// Copyright (C) 2020 Ferdinand Majerech
//
// This file is part of Timedilator.
//
// Timedilator is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Timedilator is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Timedilator.  If not, see <http://www.gnu.org/licenses/>.

module timedilator.tui;


import scone;


/// Program status types supported by the statusline.
enum StatusType
{
    /// Informational message about current program status.
    Info,
    /// Error message. Something failed.
    Error
}

/// Get background color to use for a given program status type.
Color bgcolor( in StatusType type )
{
    final switch( type ) with( StatusType )
    {
        case Info: return Color.greenDark;
        case Error:  return Color.red;
    }
}

/// Given a background color, get a color that can be used for readable, contrasting text.
Color contrastingTextColor( in Color color )
{
    final switch( color ) with(Color)
    {
        case black:       return Color.white;
        case red:         return Color.blackDark;
        case green:       return Color.blackDark;
        case yellow:      return Color.blackDark;
        case blue:        return Color.blackDark;
        case magenta:     return Color.blackDark;
        case cyan:        return Color.blackDark;
        case white:       return Color.blackDark;
        case blackDark:   return Color.white;
        case redDark:     return Color.white;
        case greenDark:   return Color.white;
        case yellowDark:  return Color.white;
        case blueDark:    return Color.white;
        case magentaDark: return Color.white;
        case cyanDark:    return Color.white;
        case whiteDark:   return Color.blackDark;
        case same:        return Color.same;
        case unknown:     return Color.unknown;
        case initial:     return Color.initial;
    }
}


/** A very light wrapper over scone.
 *
 * Provides access to input events, drawing operations and basic TUI features
 * like a statusline.
 *
 * Buffers rendered characters to reduce scone allocations and enforces constant
 * maximum framerate (minimum frame times).
 */
class TUI
{
    import std.datetime;
    import std.stdio;
    import core.thread;
private:
    // TODO refactor all status-related members to a struct

    /// Pane status message; multiple can be drawn on the right end of statusline.
    struct PaneStatus
    {
        /// Background color of the message (foreground will be a constrasting color).
        Color color;
        /// Message text.
        string text;
    }

    /// Current program status (e.g. an error).
    StatusType programStatusType;

    /// Message describing the program status (e.g. an error message).
    string programStatusMessage;

    /// Pane statuses, drawn in reverse order from the right.
    PaneStatus[] paneStatuses;


    /// Whole-screen message, if any. Usually null.
    string screenMessage;

    /// Width of the terminal.
    uint windowWidth_ = 120;

    /// Height of the terminal.
    uint windowHeight_ = 32;

    /// Desired framerate (frames per second).
    uint framerate_    = 10;


    /// Time when the previous frame ended (or for the first frame, time when `this()` was called).
    SysTime previousFrameEnd;

    /// Used for any cell buffering, mainly to buffer cells consecutively drawn by `drawCell()`.
    Cell[] cellBuffer;

    /// Column of the first cell of the current run of buffered consecutive cells.
    size_t consecutiveStartX = 0;

    /// Row of the first cell of the current run of buffered consecutive cells.
    size_t consecutiveStartY = 0;

    /// Number of consecutive cells buffered at the moment. 0 if there are no buffered cells.
    size_t consecutiveCount = 0;

    /// Index of the current frame, starting at 0.
    size_t frameIdx    = 0;

public:
    /** Construct the TUI.
     *
     * Records 'end' time for previous-to-first frame - effectively the first frame starts here.
     */
    this()
    {
        stderr.writefln( "TUI.this" );
        window.title( "timedilator" );
        window.resize( windowWidth_, windowHeight_ );
        window.clear();
        previousFrameEnd = Clock.currTime;
        cellBuffer = new Cell[4096];
    }

    /** Process input without any special handler.
     *
     * Returns: true if the program should continue to run, false if the program should quit.
     */
    bool input()
    {
        return input( (InputEvent) {} );
    }

    /** Process input using a function.
     *
     * Params:
     * processEvent = Function to call for each input event not handled by TUI
     *                (currently only inputs determining whether to quit are
     *                handled by TUI).
     *
     * Returns: true if the program should continue to run, false if the program should quit.
     */
    bool input( void delegate( InputEvent ) processEvent )
    {
        foreach( input; window.getInputs() )
        {
            // C-c
            if( input.key == SK.c && input.hasControlKey( SCK.ctrl ) ) { return false; }
            // q
            if( input.key == SK.q )                                    { return false; }
            processEvent( input );
        }
        return true;
    }

    /** Finish a TUI frame, writing out all drawn text to the terminal.
     *
     * After drawing the terminal, sleeps if the time since last frame does
     * not correspond to the framerate yet. If you want to use this time to
     * do some useful work, look at `framePercentRemaining()`.
     */
    void frame()
    {
        if( frameIdx % framerate == 0)
        {
            stderr.writefln( "TUI.frame %s", frameIdx );
        }

        // Flush any buffered consecutive cells
        flushConsecutiveCells();
        // Draw the screen message, if any (overriding anything else drawn), and the statusline.
        drawScreenMessage();
        drawStatusLine();

        // 'Blit' the screen.
        window.print();
        // Clear to prepare for the next frame.
        window.clear();
        // Handle any window resizes.
        updateWindowSize();

        // Sleep if we're not at desired frame end time yet.
        const time                 = Clock.currTime;
        const frameDurationSoFar   = time - previousFrameEnd;
        const desiredFrameDuration = dur!"usecs"(1000000 / framerate_ );
        if( frameDurationSoFar < desiredFrameDuration )
        {
            Thread.sleep( desiredFrameDuration - frameDurationSoFar );
        }
        // Save frame end so we can use it to determine next frame duration.
        previousFrameEnd = Clock.currTime;
        ++frameIdx;
    }

    /** Set the current program-wide status - controls the left side of the statusline.
     *
     * The program status lives between frames, unlike pane statuses.
     * TODO: consider replacing this with an immediate-mode modular approach like pane statuses.
     *
     * Params:
     * type    = Status type (e.g. error).
     * message = Status message (text to show).
     */
    void programStatus( in StatusType type, in string message )
    {
        programStatusType    = type;
        programStatusMessage = message;
    }

    /** Add a pane status - controls the right side of the statusline.
     *
     * Pane statuses are cleared and have to be readded on every frame.
     *
     * Pane statuses are drawn to the right side of the statusline in reverse 
     * order of adding, up to the point where all pane statuses are drawn
     * or the next pane status would collide with the program status
     * (so program status takes priority).
     *
     * Params:
     * color = Background color of the status text.
     * text  = Status text to draw.
     */
    void addPaneStatus( in Color color, in string text )
    {
        paneStatuses ~= PaneStatus( color, text );
    }

    // TODO replace with a pane + a pane manager API to add a focused fullscreen pane.
    /** Set a full-screen message.
     *
     * If set, only the message and the statusline will be drawn, with no panes.
     *
     * The message lives between frames.
     *
     * Should be used mainly for fatal error messages.
     *
     * Params:
     * message = Message to show. Can be multi-line.
     */
    void screen( in string message )
    {
        screenMessage = message;
    }

    /** Get the time remaining in current frame (based on framerate), in percent.
     *
     * Can be checked to see if we have time to do any optional work in the current frame.
     */
    long framePercentRemaining() const
    {
        const frameDurationSoFar   = Clock.currTime - previousFrameEnd;
        const desiredFrameDuration = dur!"usecs"(1000000 / framerate_ );
        const framePercentSoFar    = (100 * frameDurationSoFar) / desiredFrameDuration;
        return 100 - framePercentSoFar;
    }

    /// Get window width (columns).
    size_t windowWidth() const { return windowWidth_; }

    /// Get window height (rows).
    size_t windowHeight() const { return windowHeight_; }

    /** Set framerate (in frames per second) to specified value.
     *
     * Note that if you set this too high, TUI may not be able to draw that fase.
     */
    void framerate( const uint rate ) { framerate_ = rate; }

    /// Get current framerate.
    uint framerate() const { return framerate_; }

    /// Get the index of the current frame.
    size_t frameIndex() const { return frameIdx; }

    /** Draw a background rectangle for a pane (really just fills a rectangle with specified color).
     *
     * Params:
     * color  = Background color to draw with.
     * x      = First column to draw on.
     * y      = First row to draw on.
     * width  = Width of the rectangle in columns.
     * height = Height of the rectangle in rows.
     */
    void drawPaneBackground( in Color color, in size_t x, in size_t y, in size_t width, in size_t height )
    {
        const maxY = y + height - 1;
        assert( maxY == 0 || maxY + 1 <= windowHeight_,
                "drawPaneBackground trying to draw over statusline" );

        // Nothing to draw.
        if( width == 0 || height == 0 ) { return; }

        // Create a buffer for a single row and reuse it (reduces allocations in scone)
        auto backgroundCells = cells( width );
        backgroundCells[0 .. width] = Cell( ' ', Color.same.foreground, color.background );
        // Write empty pane background.
        foreach( row; y .. y + height )
        {
            window.write( x, row, backgroundCells );
        }
    }

    /** Set cell colors/character directly.
     *
     * Params:
     * bg        = Background color of the cell.
     * fg        = Foreground (character) color of the cell.
     * character = Character to draw. Currently supports only 8-bit chars (need newer scone for unicode).
     * x         = Column of the cell to edit.
     * y         = Row of the cell to edit.
     *
     * Note:
     * Optimized for consecutive calls in a row (same y, incrementing x).
     */
    void drawCell( in Color bg, in Color fg, in dchar character, in size_t x, in size_t y )
    {
        assert( character <= char.max, "dchars not yet supported" );

        // There is a buffered sequence of consecutive cells, and this cell can be added to it.
        if( consecutiveCount > 0 &&  y == consecutiveStartY && x == consecutiveStartX + consecutiveCount )
        {
            if( consecutiveCount + 1 > cellBuffer.length )
            {
                cellBuffer.length = (consecutiveCount + 1) * 2;
            }
            cellBuffer[consecutiveCount++] = Cell( cast(char)character, fg.foreground, bg.background );
            return;
        }
        // The new cell is not consecutive - flush consecutive cells if any.
        flushConsecutiveCells();
        // Start a new sequence of consecutive cells.
        consecutiveStartX = x;
        consecutiveStartY = y;
        cellBuffer[consecutiveCount++] = Cell( cast(char)character, fg.foreground, bg.background );
    }

private:
    /** Flush buffered consecutive cells, if any.
     *
     * The least-allocating way scone can draw cells is if we pass an array of scone.Cell[] to it.
     */
    void flushConsecutiveCells()
    {
        if( consecutiveCount > 0 )
        {
            window.write( consecutiveStartX, consecutiveStartY, cellBuffer[0 .. consecutiveCount]);
            consecutiveCount = 0;
        }
    }

    /** Draw the screen message, if any.
     */
    void drawScreenMessage()
    {
        window.write( 0, 0, screenMessage );
    }

    /** Draw the statusline.
     *
     * Program status from the left, pane statuses from the right.
     */
    void drawStatusLine()
    {
        // Program status first
        const y  = windowHeight_ - 1;
        const programFg = Color.white;
        const programBg = programStatusType.bgcolor;
        if( programStatusMessage )
        {
            window.write( 0, y, programFg.foreground, programBg.background, programStatusMessage );
        }

        auto padding = cells( windowWidth_ - programStatusMessage.length );
        padding[] = Cell( ' ', programFg.foreground, programBg.background );
        window.write( programStatusMessage.length, y, padding );

        // Draw pane statuses, from right to left.
        size_t baseX = windowWidth_;
        foreach( status; paneStatuses )
        {
            baseX -= status.text.length;
            // Stop if we collide with the program status message.
            if( baseX <= programStatusMessage.length )
            {
                break;
            }
            window.write( baseX, y, status.color.contrastingTextColor.foreground, status.color.background, status.text );
            // Gap of 1 cell between the statuses.
            baseX -= 1;
        }

        paneStatuses.length = 0;
    }

    /// Detect if terminal size has changed, and if so, resize the window.
    void updateWindowSize()
    {
        // TODO Unix only. Should be in scope 0.3 .
        version(Posix)
        {
            import core.sys.posix.sys.ioctl : ioctl, winsize, TIOCGWINSZ;
            import core.sys.posix.unistd : read, STDOUT_FILENO;
            winsize w;
            ioctl( STDOUT_FILENO, TIOCGWINSZ, &w );
            if( w.ws_col != windowWidth_ || w.ws_row != windowHeight_ )
            {
                window.resize( w.ws_col, w.ws_row );
                windowWidth_  = w.ws_col;
                windowHeight_ = w.ws_row;
            }
        }
    }

    /** Get a cell buffer of specified width (from `cellBuffer`), allocating if unavoidable.
     *
     * Flushes any buffered consecutive cells, since those also use cellBuffer.
     */
    Cell[] cells( const size_t width )
    {
        flushConsecutiveCells();
        if( cellBuffer.length < width )
        {
            cellBuffer.length = width * 2;
        }
        return cellBuffer[0 .. width];
    }
}
